package com.bank.mega.validator;

public class EmailValidator {
	  public static boolean isValid(String email) {
		  if(email==null) {
			  return false;
		  }
		  
	      String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
	      return email.matches(regex);
	   }
	   public static void main(String[] args) {
	      String email = "BIMASEBAYANG11@GMAIL.COM";
	      System.out.println("The E-mail ID is: " + email);
	      System.out.println("Is the above E-mail ID valid? " + isValid(email));
	   }
}
