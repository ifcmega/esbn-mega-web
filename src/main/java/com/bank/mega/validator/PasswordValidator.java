package com.bank.mega.validator;

public class PasswordValidator {
	   public static boolean isValid(String password) {
		  if(password==null) {
			  return false;
		  }
		  
	      String regex = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=.,/?*()_-|<>])(?=\\S+$).{8,}$";
	      return password.matches(regex);
	   }
	   public static void main(String[] args) {
	      String email = "1bBdsdsd>";
	      System.out.println("The E-mail ID is: " + email);
	      System.out.println("Is the above E-mail ID valid? " + isValid(email));
	   }
}
