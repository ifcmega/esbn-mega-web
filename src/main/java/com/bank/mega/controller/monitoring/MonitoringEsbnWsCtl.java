package com.bank.mega.controller.monitoring;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.bank.mega.bean.EsbnAllSeriProductEarlyRedemption;
import com.bank.mega.bean.EsbnAllSeriProductPemesanan;
import com.bank.mega.bean.EsbnInfoUser;
import com.bank.mega.bean.EsbnPemesananEarlyRedemption;
import com.bank.mega.bean.EsbnPemesananTransaksi;
import com.bank.mega.bean.EsbnRekeningDana;
import com.bank.mega.bean.EsbnRekeningSuratBerharga;
import com.bank.mega.bean.auth.TokenWrapper;
import com.bank.mega.security.HttpRestResponse;
import com.bank.mega.service.BaseService;
import com.google.gson.Gson;

@RestController
@RequestMapping("/monitoring")
public class MonitoringEsbnWsCtl extends BaseService {

	// --Monitoring Transaction--//

	@GetMapping("/get-all-seri")
	public Map<String, Object> getAllSeriProducts(Authentication auth) {
		Map<String, Object> map = new HashMap<>();

		List<EsbnAllSeriProductPemesanan> allSeriProducts = new ArrayList<>();
		EsbnInfoUser esbnInfoUser = new EsbnInfoUser();

		TokenWrapper tokenWrapper = getMyToken(auth);
		HttpRestResponse responseSeri = wsBodyEsbnWithoutBody("/v1/seri/offer", "GET", null,
				tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		HttpRestResponse responseInfo = wsBodyEsbnWithoutBody("/v1/investor/" + auth.getName().toUpperCase(), "GET", null,
				tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		try {
			allSeriProducts = mapperJsonToListDto(responseSeri.getBody(), EsbnAllSeriProductPemesanan.class);
			esbnInfoUser = mapperJsonToSingleDto(responseInfo.getBody(), EsbnInfoUser.class);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		List<EsbnAllSeriProductPemesanan> allSeriOnSelected = new ArrayList<>();
		for (EsbnAllSeriProductPemesanan seri : allSeriProducts) {
			HttpRestResponse existingTrx = wsBodyEsbnWithoutBody("/v1/pemesanan/seri/"+seri.getId()+"/"+auth.getName(), "GET", null,
					tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
			try {
				List<EsbnPemesananTransaksi> transaksis = mapperJsonToListDto(existingTrx.getBody(), EsbnPemesananTransaksi.class);
			
				
				if(transaksis!=null&&transaksis.size()>0) {
					allSeriOnSelected.add(seri);
				}
			 } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		map.put("allSeriProducts", allSeriOnSelected);
		map.put("esbnInfoUser", esbnInfoUser);
		return map;
	}

	/*
	 * @PostMapping("/getKuotaSeri") public Map<String, Object>
	 * resultKuota(@RequestBody String idSeri, Authentication authorization) {
	 * Map<String, Object> map = new HashMap<>(); TokenWrapper tokenWrapper =
	 * getMyToken(authorization); HttpRestResponse responseKuota =
	 * wsBodyEsbnWithoutBody("/v1/Kuota/" + idSeri + "/" +
	 * authorization.getName().toUpperCase(), "GET", null,
	 * tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token()); map =
	 * mapperJsonToHashMap(responseKuota.getBody()); return map; }
	 */

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@PostMapping("/search-monitoringPemesanan")
	public Map<String, Object> resultSearchBySid(@RequestBody Map<String, String> mapReq,
			Authentication authorization) {
		List<EsbnPemesananTransaksi> listEsbnPemesananTrx = new ArrayList<>();
		String idSeri = mapReq.get("idSeri");
		String pages = mapReq.get("pages");
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> mapBody = new HashMap<>();
		Map<String, Object> mapdana = new HashMap<>();
		TokenWrapper tokenWrapper = getMyToken(authorization);
		HttpRestResponse response = wsBodyEsbnWithoutBody(
				"/v1/pemesanan/seri/" + idSeri + "?IdStatus=&PageNumber="+pages+
				"&PageSize=10&Search="+authorization.getName().toUpperCase()
				+"&Sort=-"+authorization.getName().toUpperCase()+"&TglPemesanan=",
				"GET", null, tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());

		try {
			// listEsbnPemesananTrx = mapperJsonToListDto(response.getBody(),
			// EsbnPemesananTransaksi.class);
			mapBody = mapperJsonToHashMap(response.getBody());

			int pageNumber = (int) mapBody.get("PageNumber");
			int pagesize = (int) mapBody.get("PageSize");
			int totalPage = (int) mapBody.get("TotalPage");
			int totalRecord = (int) mapBody.get("TotalRecord");

			List<Map<String, String>> listTrx = (List<Map<String, String>>) mapBody.get("Records");
 
			for (Map maps : listTrx) {
				EsbnPemesananTransaksi trx = new EsbnPemesananTransaksi();
				mapdana=  (Map<String, Object>) maps.get("RekeningDana");
				EsbnRekeningDana dana = mapperHashmapToSingleDto(mapdana, EsbnRekeningDana.class);
				
				trx.setRekeningDana(dana);
				trx.setKodePemesanan((String) maps.get("KodePemesanan"));

				Double kuponDouble = (Double) maps.get("TingkatKupon");
				BigDecimal kupon = BigDecimal.valueOf(kuponDouble);
				trx.setTingkatKupon(kupon.setScale(2, BigDecimal.ROUND_HALF_UP));

				trx.setTglBayarKupon((String) maps.get("TglBayarKupon"));
				trx.setTglJatuhTempo((String) maps.get("TglJatuhTempo"));
				trx.setTglSetelmen((String) maps.get("TglSetelmen"));
				trx.setTglPemesanan((String) maps.get("TglPemesanan"));

				Double nominalDouble = (Double) maps.get("Nominal");
				BigDecimal nominal = BigDecimal.valueOf(nominalDouble);
				trx.setNominal(nominal.toBigInteger());

				Double kuponPertamaDouble = (Double) maps.get("NominalKuponPertama");
				BigDecimal kuponPertama = BigDecimal.valueOf(kuponPertamaDouble);
				trx.setNominalKuponPertama(kuponPertama.toBigInteger());

				trx.setKodeBilling((String) maps.get("KodeBilling"));
				trx.setNTPN((String) maps.get("NTPN"));
				trx.setStatus((String) maps.get("Status"));
				trx.setNamaInvestor((String) maps.get("NamaInvestor"));
				trx.setSeri((String) maps.get("Seri"));

				Double nominalKuponDouble = (Double) maps.get("NominalKupon");
				BigDecimal nominalKupon = BigDecimal.valueOf(nominalKuponDouble);
				trx.setNominalKupon(nominalKupon.toBigInteger());

				Double nominalKuponPerunitDouble = (Double) maps.get("NominalKuponPerUnit");
				BigDecimal nominalKuponPerunit = BigDecimal.valueOf(nominalKuponPerunitDouble);
				trx.setNominalKuponPerUnit(nominalKuponPerunit.toBigInteger());

				Double nominalPerunitDouble = (Double) maps.get("Nominal");
				BigDecimal nominalPerunit = BigDecimal.valueOf(nominalPerunitDouble);
				trx.setNominalPerUnit(nominalPerunit.toBigInteger());

				Double redeemDouble = (Double) maps.get("Redeem");
				BigDecimal redeem = BigDecimal.valueOf(redeemDouble);
				trx.setRedeem(redeem.toBigInteger());

				Double sisaKepemilikanDouble = (Double) maps.get("SisaKepemilikan");
				BigDecimal sisaKepemilikan = BigDecimal.valueOf(sisaKepemilikanDouble);
				trx.setSisaKepemilikan(sisaKepemilikan.toBigInteger());

				trx.setIdStatus((String) maps.get("IdStatus"));
				
				String tglLahir = (String) maps.get("BatasWaktuBayar");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				SimpleDateFormat output = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				java.util.Date d = sdf.parse(tglLahir);
				String formattedDate = output.format(d);
				
				trx.setBatasWaktuBayar(formattedDate);
				trx.setSid((String) maps.get("Sid"));

				Integer intIdSeri = (Integer) maps.get("IdSeri");
				trx.setIdSeri(intIdSeri.longValue());

				Integer intIdRekDana = (Integer) maps.get("IdRekDana");
				trx.setIdRekDana(BigInteger.valueOf(intIdRekDana.longValue()));

				Integer intIdRekSb = (Integer) maps.get("IdRekSb");
				trx.setIdRekSb(BigInteger.valueOf(intIdRekSb.longValue()));

				trx.setCreatedBy((String) maps.get("CreatedBy"));
				listEsbnPemesananTrx.add(trx);
			}

			map.put("pageNumber", pageNumber);
			map.put("pagesize", pagesize);
			map.put("totalPage", totalPage);
			map.put("totalRecord", totalRecord);
			map.put("listEsbnPemesananTrx", listEsbnPemesananTrx);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return map;
	}
	// --Monitoring Transaction--//

	// --Monitoring Early Redeemption--//

	@GetMapping("/get-all-seriRedeem")
	public Map<String, Object> getAllSeriRedeem(Authentication auth) {
		Map<String, Object> map = new HashMap<>();
		List<EsbnAllSeriProductEarlyRedemption> allSeriProductEarlyRedemptions = new ArrayList<>();
		EsbnInfoUser esbnInfoUser = new EsbnInfoUser();
		TokenWrapper tokenWrapper = getMyToken(auth);
		HttpRestResponse responseSeri = wsBodyEsbnWithoutBody("/v1/seri/redeem/offer", "GET", null,
				tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		HttpRestResponse responseInfo = wsBodyEsbnWithoutBody("/v1/investor/" + auth.getName().toUpperCase(), "GET", null,
				tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		try {
			allSeriProductEarlyRedemptions = mapperJsonToListDto(responseSeri.getBody(),
					EsbnAllSeriProductEarlyRedemption.class);
			esbnInfoUser = mapperJsonToSingleDto(responseInfo.getBody(), EsbnInfoUser.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<EsbnAllSeriProductEarlyRedemption> allSeriOnSelected = new ArrayList<>();
		for (EsbnAllSeriProductEarlyRedemption seri : allSeriProductEarlyRedemptions) {
			HttpRestResponse existingTrx = wsBodyEsbnWithoutBody("/v1/redemption/seri/"+seri.getId()+"/"+auth.getName(), "GET", null,
					tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
			try {
				List<EsbnAllSeriProductEarlyRedemption> transaksis = mapperJsonToListDto(existingTrx.getBody(), EsbnAllSeriProductEarlyRedemption.class);
				System.err.println("transaksi early : " + new Gson().toJson(transaksis));
				List<EsbnAllSeriProductEarlyRedemption> earlyRedemptions = new ArrayList<>(transaksis);
				for (EsbnAllSeriProductEarlyRedemption early : earlyRedemptions) {
					if(early.getSeri()==null) {
						transaksis.remove(early);
					}
				}
				if(transaksis!=null&&transaksis.size()>0) {
					System.err.println("seri nya " + seri.getSeri() + " masuk, karena ukuran : " + transaksis.size());
					allSeriOnSelected.add(seri);
				}
			 } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		map.put("allSeriProducts", allSeriOnSelected);
		map.put("esbnInfoUser", esbnInfoUser);
		return map;
	}

	/*
	 * @PostMapping("/getKuotaRedeem") public Map<String, Object>
	 * resultKuotaRedeem(@RequestBody String idSeri, Authentication authorization) {
	 * Map<String, Object> map = new HashMap<>(); TokenWrapper tokenWrapper =
	 * getMyToken(authorization); HttpRestResponse responseKuota =
	 * wsBodyEsbnWithoutBody("/v1/Kuota/" + idSeri + "/" +
	 * authorization.getName().toUpperCase(), "GET", null,
	 * tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token()); map =
	 * mapperJsonToHashMap(responseKuota.getBody()); return map; }
	 */

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping("/search-monitoringEarlyRedemption")
	public Map<String, Object> resultSearchBySidandIdSeri(@RequestBody Map<String, String> mapReq,
			Authentication authorization) {
		String idSeri = mapReq.get("idSeri");
		String pages = mapReq.get("pages");
		List<EsbnPemesananEarlyRedemption> listEsbnPemesananEarlyTrx = new ArrayList<>();
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> mapBody = new HashMap<>();
		TokenWrapper tokenWrapper = getMyToken(authorization);

		HttpRestResponse response = wsBodyEsbnWithoutBody(
				"/v1/redemption/seri/" + idSeri + "?PageNumber=" +pages+"&PageSize=10&Search="+authorization.getName().toUpperCase() 
				+"&Sort=-"+authorization.getName().toUpperCase()+"&TglRedeem=",
				"GET", null, tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());

		try {
			// listEsbnPemesananTrx = mapperJsonToListDto(response.getBody(),
			// EsbnPemesananEarlyRedemption.class);
			mapBody = mapperJsonToHashMap(response.getBody());

			int pageNumber = (int) mapBody.get("PageNumber");
			int pagesize = (int) mapBody.get("PageSize");
			int totalPage = (int) mapBody.get("TotalPage");
			int totalRecord = (int) mapBody.get("TotalRecord");

			List<Map<String, String>> listTrx = (List<Map<String, String>>) mapBody.get("Records");
			for (Map maps : listTrx) {
				EsbnPemesananEarlyRedemption trx = new EsbnPemesananEarlyRedemption();
				trx.setNamaInvestor((String) maps.get("NamaInvestor"));
				trx.setKodeRedeem((String) maps.get("KodeRedeem"));
				trx.setSeri((String) maps.get("Seri"));
				trx.setTglSetelmen((String) maps.get("TglSetelmen"));

				Double sisaKepemilikanDouble = (Double) maps.get("SisaKepemilikan");
				BigDecimal sisaKepemilikan = BigDecimal.valueOf(sisaKepemilikanDouble);
				trx.setSisaKepemilikan(sisaKepemilikan.toBigInteger());

				trx.setStatus((String) maps.get("Status"));
				trx.setCreatedBy((String) maps.get("CreatedBy"));
				trx.setTglRedeem((String) maps.get("TglRedeem"));

				Double kelipatanRedeemDouble = (Double) maps.get("KelipatanRedeem");
				BigDecimal kelipatanRedeem = BigDecimal.valueOf(kelipatanRedeemDouble);
				trx.setKelipatanRedeem(kelipatanRedeem.toBigInteger());

				Double minRedeemDouble = (Double) maps.get("MinimalRedeem");
				BigDecimal minRedeem = BigDecimal.valueOf(minRedeemDouble);
				trx.setMinimalRedeem(minRedeem.toBigInteger());

				Double maxRedeemDouble = (Double) maps.get("MaksimalRedeem");
				BigDecimal maxRedeem = BigDecimal.valueOf(maxRedeemDouble);
				trx.setMaksimalRedeem(maxRedeem.toBigInteger());

				Double redeemableDouble = (Double) maps.get("Redeemable");
				BigDecimal redeemable = BigDecimal.valueOf(redeemableDouble);
				trx.setRedeemable(redeemable.toBigInteger());

				trx.setKodePemesanan((String) maps.get("KodePemesanan"));
				trx.setSid((String) maps.get("Sid"));

				Double nominalDouble = (Double) maps.get("Nominal");
				BigDecimal nominal = BigDecimal.valueOf(nominalDouble);
				trx.setNominal(nominal.toBigInteger());
				listEsbnPemesananEarlyTrx.add(trx);
			}

			map.put("pageNumberEarly", pageNumber);
			map.put("pagesizeEarly", pagesize);
			map.put("totalPageEarly", totalPage);
			map.put("totalRecordEarly", totalRecord);
			map.put("listEsbnPemesananEarlyTrx", listEsbnPemesananEarlyTrx);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return map;
	}

	// --Monitoring Early Redeemption--//
}
