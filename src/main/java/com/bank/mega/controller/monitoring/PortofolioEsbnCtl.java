package com.bank.mega.controller.monitoring;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bank.mega.bean.EsbnMonitoringEarlyRedemption;
import com.bank.mega.service.BaseService;

@Controller
public class PortofolioEsbnCtl extends BaseService{

	
	@RequestMapping("/portofolio")
	public String esbnMonitoringPemesanan(Model model, Authentication auth, HttpServletRequest request,
			HttpServletResponse response, EsbnMonitoringEarlyRedemption esbnMonitoringEarlyRedemption) {
		informationHeader(model, auth);
		if(isMenuAccessable(auth)) {
		return checkSessionValidity("monitoring/portofolio-esbn", auth, request, response);
		}
		else {
			return checkSessionValidity("monitoring/noreg/portofolio-esbn-noreg", auth, request, response);
		}
	}
}
