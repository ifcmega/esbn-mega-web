package com.bank.mega.controller.shared;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bank.mega.bean.EsbnEmailComplete;
import com.bank.mega.bean.EsbnPemesananTransaksi;
import com.bank.mega.bean.PagingStatusPembayaranBean;
import com.bank.mega.bean.StatusPembayaranBean;
import com.bank.mega.bean.TblEsbnPemesananTransaksiBean;
import com.bank.mega.bean.auth.TokenWrapper;
import com.bank.mega.bean.principal.BasePrincipalSessions;
import com.bank.mega.model.TblUserEsbn;
import com.bank.mega.security.HttpRestResponse;
import com.bank.mega.service.BaseService;
import com.google.gson.Gson;

@RestController
@RequestMapping("/shared")
public class SomethingSharedOutsideSecurity extends BaseService {

	private EsbnEmailComplete getEmailTransaksi(String kodePemesanan, String tanggalJamBayar,
			TokenWrapper tokenWrapper) {
		EsbnEmailComplete email = new EsbnEmailComplete();
		String authorization = tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token();
		HttpRestResponse restKodePemesanan = wsBodyEsbnWithoutBody("/v1/pemesanan/" + kodePemesanan, "GET", null,
				authorization);
		try {
			EsbnPemesananTransaksi transaksi = mapperJsonToSingleDto(restKodePemesanan.getBody(),
					EsbnPemesananTransaksi.class);
			TblUserEsbn esbn = getTblUser(transaksi.getSid(), tokenWrapper);
			email.setEmail(esbn.getUserEmail());
			email.setKodeNtpn(transaksi.getNTPN());
			email.setKodePemesanan(transaksi.getKodePemesanan());
			email.setNamaSeri(transaksi.getSeri());
			email.setNamaUser(esbn.getUserName());
			email.setNominalPemesanan(changeFormatNumberMoney(transaksi.getNominal()));
			email.setWaktuPembayaran(tanggalJamBayar);
			email.setUserSid(esbn.getUserSid());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return email;
	}

	
	private List<StatusPembayaranBean> getAllTransaksiCompleteOrder(TokenWrapper tokenWrapper) {
		String firstMeta = "/v1/pemesanan/statuspembayaran?PageNumber=1&PageSize=1&TglPembayaranAkhir=&TglPembayaranAwal=&Sort=";
		String authorization = tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token();
		HttpRestResponse response = wsBodyEsbnWithoutBody(firstMeta, "GET", null, authorization);
		PagingStatusPembayaranBean bean = new PagingStatusPembayaranBean();
		try {
			bean = mapperJsonToSingleDto(response.getBody(), PagingStatusPembayaranBean.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String secondMeta = "/v1/pemesanan/statuspembayaran?PageNumber=1&PageSize=" + bean.getTotalPage()
				+ "&TglPembayaranAkhir=&TglPembayaranAwal=&Sort=";

		HttpRestResponse responseSecond = wsBodyEsbnWithoutBody(secondMeta, "GET", null, authorization);
		PagingStatusPembayaranBean beanSecond = new PagingStatusPembayaranBean();

		try {
			beanSecond = mapperJsonToSingleDto(responseSecond.getBody(), PagingStatusPembayaranBean.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		List<StatusPembayaranBean> statusPembayaranBeans = new ArrayList<>();
		if (beanSecond != null) {
			statusPembayaranBeans = beanSecond.getRecords();
		}
		Map<String, String> headerMap = new HashMap<>();
		headerMap.put("Authorization", authorization);

		HttpRestResponse responseDatabase = wsBody(wmsParentalWrapper + "/transaksi/get-all-incomplete", null,
				HttpMethod.GET, headerMap);
		List<TblEsbnPemesananTransaksiBean> beanDb = new ArrayList<>();
		List<StatusPembayaranBean> statusPembayaranBeansComplete = new ArrayList<>();
		try {
			beanDb = mapperJsonToListDto(responseDatabase.getBody(), TblEsbnPemesananTransaksiBean.class);

			for (TblEsbnPemesananTransaksiBean bdb : beanDb) {
				if (statusPembayaranBeans.stream()
						.filter(o -> o.getKodePemesanan().equalsIgnoreCase(bdb.getTransaksiPemesanan())).findFirst()
						.isPresent()) {
					StatusPembayaranBean spb = statusPembayaranBeans.stream()
							.filter(o -> o.getKodePemesanan().equalsIgnoreCase(bdb.getTransaksiPemesanan())).findFirst()
							.get();
					spb.setTrxId(bdb.getTrxId()+"");
					statusPembayaranBeansComplete.add(spb);
					bdb.setTransaksiStatus("Completed Order");
					bdb.setTransaksiCode("4");
					@SuppressWarnings("unused")
					HttpRestResponse responseUpdate = wsBody
							(wmsParentalWrapper + "/transaksi/update/transaksi-status/"+bdb.getTransaksiPemesanan(), bdb,
							HttpMethod.PUT, headerMap);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return statusPembayaranBeansComplete;
	}
	
	
	@PostMapping("/StatusPembayaran")
	private String resultWmsUpdateStatusPembayaran(@RequestBody List<Map<String, Object>> wmsUpdateStatusPembayarans) {
		String tandaPengenal = "ACOnuT3Y6bbprsCoAucgYHm97C4FtDQAYs41Bv658GOpevBGEjL0ZGTdo04FhdMz0C0hBz9QzXkZ6NJqa04GaxRGjy9A8D8i";
		TokenWrapper tokenWrapper = getMyToken(tandaPengenal);
		Map<String, Object> maps = new HashMap<>();
		if (tokenWrapper.getReasonCode() == 401 || tokenWrapper.getReasonCode() == 403) {
			return new Gson().toJson(tokenWrapper);
		}
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("tanda_pengenal", tandaPengenal);
		HttpRestResponse resultUpdate = wsBody(wmsParentalWrapper + "/shared/StatusPembayaran",
				wmsUpdateStatusPembayarans, HttpMethod.POST, headerMap);
		maps = mapperJsonToHashMap(resultUpdate.getBody());
		for (Map<String, Object> map : wmsUpdateStatusPembayarans) {
			String kodePemesanan = (String) map.get("KodePemesanan");
			String TglJamPembayaran = (String) map.get("TglJamPembayaran");
			EsbnEmailComplete emailComplete = getEmailTransaksi(kodePemesanan, TglJamPembayaran, tokenWrapper);
			
			if(emailComplete==null|| emailComplete.getUserSid()==null) {
				maps.put("reason", "Transaksi Tidak ditemukan");
				maps.put("success", false);
			}
		
			
			else	if (!sendEmailComplete(emailComplete)) {
				maps.put("reason", REASON + " in kode pemesanan : " + kodePemesanan);
				maps.put("success", false);
			}
		}
		
		Map<String, Object> mapReturn = mapperJsonToHashMap(resultUpdate.getBody());

		return new Gson().toJson(mapReturn.get("wmsReturn"));
	}
	
	@GetMapping("/StatusPembayaran/new")
	private String resultWmsUpdateStatusPembayaran() {
		String tandaPengenal = "ACOnuT3Y6bbprsCoAucgYHm97C4FtDQAYs41Bv658GOpevBGEjL0ZGTdo04FhdMz0C0hBz9QzXkZ6NJqa04GaxRGjy9A8D8i";
		TokenWrapper tokenWrapper = getMyToken(tandaPengenal);
		List<StatusPembayaranBean> wmsUpdateStatusPembayarans = getAllTransaksiCompleteOrder(tokenWrapper);
		Map<String, Object> maps = new HashMap<>();
		if (tokenWrapper.getReasonCode() == 401 || tokenWrapper.getReasonCode() == 403) {
			return new Gson().toJson(tokenWrapper);
		}
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("tanda_pengenal", tandaPengenal);
		
		List<Map<String, Object>> wmsUpdater = new ArrayList<>();
		for (StatusPembayaranBean map : wmsUpdateStatusPembayarans) {
			Map<String, Object> mapo = new HashMap<>();
			mapo.put("TrxId", map.getTrxId());
			mapo.put("TglJamPembayaran", map.getTglJamPembayaran());
			mapo.put("NTPN", map.getNTPN());
			mapo.put("NTB", map.getNTB());
			mapo.put("BankPersepsi", map.getBankPresepsi());
			mapo.put("ChannelPembayaran", map.getChannelPembayaran());
			mapo.put("KodeBilling", map.getKodeBilling());
			mapo.put("KodePemesanan", map.getKodePemesanan());
			wmsUpdater.add(mapo);
		}
		HttpRestResponse resultUpdate = wsBody(wmsParentalWrapper + "/shared/StatusPembayaran",
				wmsUpdater, HttpMethod.POST, headerMap);
		maps = mapperJsonToHashMap(resultUpdate.getBody());
		for (StatusPembayaranBean map : wmsUpdateStatusPembayarans) {
			String kodePemesanan = map.getKodePemesanan();
			String TglJamPembayaran = map.getTglJamPembayaran();
			EsbnEmailComplete emailComplete = getEmailTransaksi(kodePemesanan, TglJamPembayaran, tokenWrapper);
			if (!sendEmailComplete(emailComplete)) {
				maps.put("reason", REASON + " in kode pemesanan : " + kodePemesanan);
				maps.put("success", false);
			}
		}

		return new Gson().toJson(maps);
	}
	
//	@Scheduled(fixedDelay = 1000)
//	public void scheduleFixedDelayTask() {
//	    System.out.println(
//	      "Fixed delay task - " + System.currentTimeMillis() / 1000);
//	}
	
	@Scheduled(fixedDelay = 1000)
	private void resultWmsUpdateStatusPembayaranScheduled() {
		String tandaPengenal = "ACOnuT3Y6bbprsCoAucgYHm97C4FtDQAYs41Bv658GOpevBGEjL0ZGTdo04FhdMz0C0hBz9QzXkZ6NJqa04GaxRGjy9A8D8i";
		TokenWrapper tokenWrapper = getMyToken(tandaPengenal);
		List<StatusPembayaranBean> wmsUpdateStatusPembayarans = getAllTransaksiCompleteOrder(tokenWrapper);
		Map<String, Object> maps = new HashMap<>();
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("tanda_pengenal", tandaPengenal);
		
		List<Map<String, Object>> wmsUpdater = new ArrayList<>();
		for (StatusPembayaranBean map : wmsUpdateStatusPembayarans) {
			Map<String, Object> mapo = new HashMap<>();
			mapo.put("TrxId", map.getTrxId());
			mapo.put("TglJamPembayaran", map.getTglJamPembayaran());
			mapo.put("NTPN", map.getNTPN());
			mapo.put("NTB", map.getNTB());
			mapo.put("BankPersepsi", map.getBankPresepsi());
			mapo.put("ChannelPembayaran", map.getChannelPembayaran());
			mapo.put("KodeBilling", map.getKodeBilling());
			mapo.put("KodePemesanan", map.getKodePemesanan());
			wmsUpdater.add(mapo);
		}
		HttpRestResponse resultUpdate = wsBody(wmsParentalWrapper + "/shared/StatusPembayaran",
				wmsUpdater, HttpMethod.POST, headerMap);
		maps = mapperJsonToHashMap(resultUpdate.getBody());
		for (StatusPembayaranBean map : wmsUpdateStatusPembayarans) {
			String kodePemesanan = map.getKodePemesanan();
			String TglJamPembayaran = map.getTglJamPembayaran();
			EsbnEmailComplete emailComplete = getEmailTransaksi(kodePemesanan, TglJamPembayaran, tokenWrapper);
			if (!sendEmailComplete(emailComplete)) {
				maps.put("reason", REASON + " in kode pemesanan : " + kodePemesanan);
				maps.put("success", false);
			}
		}
	}

	@PostMapping("/killedAllThisSession")
	public String sessionKilledYou(@RequestBody String sessionId) {
		sessionRegistry.removeSessionInformation(sessionId);
		return "Session All Killed";
	}

	@PostMapping("/get-all-web-session")
	public List<BasePrincipalSessions> getAllSessions(@RequestBody String accessId) {
		List<BasePrincipalSessions> basePrincipalSessions = new ArrayList<>();

		for (Object principal : sessionRegistry.getAllPrincipals()) {

			List<BasePrincipalSessions> list = new ArrayList<>();
			try {
				System.err.println(new Gson().toJson(sessionRegistry.getAllSessions(principal, true)));
				list = mapperJsonToListDto(new Gson().toJson(sessionRegistry.getAllSessions(principal, true)),
						BasePrincipalSessions.class);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			basePrincipalSessions.add(list.get(0));

		}

		return basePrincipalSessions;
	}
}
