package com.bank.mega.controller.otp;

import java.awt.Font;
import java.awt.font.TextAttribute;
import java.text.AttributedString;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.http.HttpMethod;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bank.mega.bean.TblSmsLogBean;
import com.bank.mega.bean.auth.TokenWrapper;
import com.bank.mega.security.HttpRestResponse;
import com.bank.mega.service.BaseService;
import com.google.gson.Gson;

@RestController
@RequestMapping("/otp-calling")
public class OtpCallerWs extends BaseService {


public String getRandomIntegerBetweenRange(double min, double max, int flex){
    String rand = "";
	for(int i = 1; i<=flex; i++) {
	double x = (int)(Math.random()*((max-min)+1))+min;
	int xInt = (int) x;
	rand += Integer.toString(xInt); 
    }
    
    return rand;
}

//@PostMapping("/create-otp/{no-phone}")
//private Map<String, Object> callYouOtpWithPhone(
//		@PathVariable(value = "no-phone",required = true) String noPhone,
//		@RequestBody String body,Authentication authentication,HttpSession session)
//{
//	String otp = getRandomIntegerBetweenRange(0, 9, 6);
//	TokenWrapper tokenWrapper =  getMyToken(authentication);
//	Map<String, String> headerMap = new HashMap<>();
//	headerMap.put("Authorization", tokenWrapper.getToken_type()+" " + tokenWrapper.getAccess_token());
//	String noHandphone = noPhone;
//	headerMap.put("secret-phone", noHandphone);
//	headerMap.put("secret-message", otp + " adalah kode verifikasi Anda. Mohon jaga keamanan data ini.");
//	System.out.println("header otp : " + new Gson().toJson(headerMap));
//	HttpRestResponse response =  wsBody(wmsParentalWrapper+"/otp-please/v2/calling-me", null, HttpMethod.GET, headerMap);
//	System.err.println(new Gson().toJson(response));
//	Map<String, Object> map = new HashMap<>();
//	
//	if(response.getBody().toUpperCase().contains("internal server error".toUpperCase()))
//	{
//		map.put("valid", true);
//		map.put("message", "Terjadi Kesalahan pada internal kami, kode verifikasi tidak dapat kami kirimkan ");
//		return map;
//	}
//
//
//	
//	String maskingPhonenumber = maskingPhoneNumber(noHandphone);
//	System.err.println(response.getBody());
//
//	map.put("valid", true);
//	map.put("reason", "OTP SENT");
//	map.put("message", "Kode OTP telah kami kirimkan ke nomor "+ maskingPhonenumber
//			+".\n"+"Anda dapat memperbaharui data Anda pada menu REGISTRASI jika sudah tidak menggunakan nomor handphone tersebut"
//			);
//	session.setAttribute(body,otp);
//	
//	TblSmsLogBean smsLogBean = new TblSmsLogBean();
//	smsLogBean.setLogPesan( otp + " adalah kode verifikasi Anda. Mohon jaga keamanan data ini.");
//	smsLogBean.setLogResend(" ");
//	smsLogBean.setLogSubject("OTP Verifikasi");
//	smsLogBean.setLogTglWaktu(new Date());
//	smsLogBean.setLogTujuan(noHandphone);
//	smsLogBean.setLogUser(authentication.getName());
//	HttpRestResponse responseSmsLog =  wsBody(wmsParentalWrapper+"/shared/save/sms-log",
//			smsLogBean, HttpMethod.POST, headerMap);
//	return map;
//}

//	@PostMapping("/create-otp")
//	private Map<String, Object> callYouOtp(@RequestBody String body,Authentication authentication,HttpSession session)
//	{
//		String otp = getRandomIntegerBetweenRange(0, 9, 6);
//		
//		//TblUserEsbn tblUserEsbn = userAuthenticationCustomLogin.findUserBySid(authentication.getName());
//	
//		TokenWrapper tokenWrapper =  getMyToken(authentication);
//		Map<String, String> headerMap = new HashMap<>();
//		headerMap.put("Authorization", tokenWrapper.getToken_type()+" " + tokenWrapper.getAccess_token());
//		String noHandphone = getTblUser(authentication).getUserPhone();	
//		headerMap.put("secret-phone", noHandphone);
//		headerMap.put("secret-message", otp + " adalah kode verifikasi Anda. Mohon jaga keamanan data ini.");
//		System.out.println("header otp : " + new Gson().toJson(headerMap));
//		HttpRestResponse response =  wsBody(wmsParentalWrapper+"/otp-please/v2/calling-me", null, HttpMethod.GET, headerMap);
//		System.err.println(new Gson().toJson(response));
//		Map<String, Object> map = new HashMap<>();
//		
//		if(response.getBody().toUpperCase().contains("internal server error".toUpperCase()))
//		{
//			map.put("valid", true);
//			map.put("message", "Terjadi Kesalahan pada internal kami, kode verifikasi tidak dapat kami kirimkan ");
//			return map;
//		}
//	
////		if(response.getStatus()!=HttpStatus.ACCEPTED) {
////			map.put("valid", true);
////			map.put("reason", "OTP SENT");
////			map.put("message", "Mohon maaf terjadi kesalahan pada internal kami, kami tidak dapat mengirim OTP anda");
////			session.setAttribute(body,otp);
////			return map;
////		}
//		
//		String maskingPhonenumber = maskingPhoneNumber(noHandphone);
//		System.err.println(response.getBody());
//	
//		
//		
//		map.put("valid", true);
//		map.put("reason", "OTP SENT");
//		map.put("message", "Kode OTP telah kami kirimkan ke nomor "+ maskingPhonenumber
//				+".\n"+"Anda dapat memperbaharui data Anda pada menu REGISTRASI jika sudah tidak menggunakan nomor handphone tersebut"
//				);
//		session.setAttribute(body,otp);
//		
//		
//		TblSmsLogBean smsLogBean = new TblSmsLogBean();
//		smsLogBean.setLogPesan( otp + " adalah kode verifikasi Anda. Mohon jaga keamanan data ini.");
//		smsLogBean.setLogResend(" ");
//		smsLogBean.setLogSubject("OTP Verifikasi");
//		smsLogBean.setLogTglWaktu(new Date());
//		smsLogBean.setLogTujuan(noHandphone);
//		smsLogBean.setLogUser(authentication.getName());
//		HttpRestResponse responseSmsLog =  wsBody(wmsParentalWrapper+"/shared/save/sms-log",
//				smsLogBean, HttpMethod.POST, headerMap);
//		return map;
//	}
	
private boolean isUnableToRequest(HttpSession session,String requestFrom,String otpStartTime,String otpTimer, String otpWrongAnswerPemesanan) {
	System.err.println(new Gson().toJson(session));
	
	if(session.getAttribute(requestFrom)==null||((String)session.getAttribute(requestFrom)).equals("")) {
		return false;
	}
	
	int timerOtp = (int) session.getAttribute(otpTimer);
	int wrongOtpAnswer = (int) session.getAttribute(otpWrongAnswerPemesanan);
	long timerOtpStart = (long) session.getAttribute(otpStartTime);
	Float porcessCallWs = (System.currentTimeMillis() - timerOtpStart) / 1000F;

	if (timerOtp <= 0 || porcessCallWs >= 60) {
		return false;
	}

	else if (wrongOtpAnswer <= 0) {
		return false;
	}
	
	else {
		return true;
	}
	
}

	@GetMapping("/get-otp-transaksiPemesanan")
	private Map<String, Object> getcallYouOtpTransaksiPemesanan(Authentication authentication, HttpSession session) {
		if(isUnableToRequest(session, "transaksiPemesanan", OTP_START_TIME_PEMESANAN, 
				OTP_TIMER_PEMESANAN, OTP_WRONG_ANSWER_PEMESANAN))
		{
			Map<String, Object> map = new HashMap<>();
			map.put("valid", true);
			map.put("message", "Mohon Maaf Anda Belum Dapat Meminta Kode OTP Saat Ini.");
			return map;
		}
		String otp = getRandomIntegerBetweenRange(0, 9, 6);

		// TblUserEsbn tblUserEsbn =
		// userAuthenticationCustomLogin.findUserBySid(authentication.getName());

		TokenWrapper tokenWrapper = getMyToken(authentication);
		Map<String, String> headerMap = new HashMap<>();
		headerMap.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());

		String noHandphone = getTblUser(authentication).getUserPhone();

		headerMap.put("secret-phone", noHandphone);
		headerMap.put("secret-message", otp + " adalah kode verifikasi Anda. Mohon jaga keamanan data ini.");
		System.out.println("header otp : " + new Gson().toJson(headerMap));
		HttpRestResponse response = wsBody(wmsParentalWrapper + "/otp-please/v2/calling-me", null, HttpMethod.GET,
				headerMap);
		System.err.println(new Gson().toJson(response));
		Map<String, Object> map = new HashMap<>();

		if (response.getBody().toUpperCase().contains("internal server error".toUpperCase())) {
			map.put("valid", true);
			map.put("message", "Terjadi Kesalahan pada internal kami, kode verifikasi tidak dapat kami kirimkan ");
			return map;
		}

//		if(response.getStatus()!=HttpStatus.ACCEPTED) {
//			map.put("valid", true);
//			map.put("reason", "OTP SENT");
//			map.put("message", "Mohon maaf terjadi kesalahan pada internal kami, kami tidak dapat mengirim OTP anda");
//			session.setAttribute(body,otp);
//			return map;
//		}

		String maskingPhonenumber = maskingPhoneNumber(noHandphone);
		System.err.println(response.getBody());

		map.put("valid", true);
		map.put("reason", "OTP SENT");
		map.put("message", "Kode OTP telah kami kirimkan ke nomor " + maskingPhonenumber + ".\n"
				+ "Anda dapat memperbaharui data Anda pada menu REGISTRASI jika sudah tidak menggunakan nomor handphone tersebut");
		session.setAttribute("transaksiPemesanan", otp);
		session.setAttribute(OTP_WRONG_ANSWER_PEMESANAN, 3);
		session.setAttribute(OTP_TIMER_PEMESANAN, 60);
		
		
		Long startOtpMilis = System.currentTimeMillis();
		session.setAttribute(OTP_START_TIME_PEMESANAN, startOtpMilis);

		TblSmsLogBean smsLogBean = new TblSmsLogBean();
		smsLogBean.setLogPesan(otp + " adalah kode verifikasi Anda. Mohon jaga keamanan data ini.");
		smsLogBean.setLogResend(" ");
		smsLogBean.setLogSubject("OTP Verifikasi");
		smsLogBean.setLogTglWaktu(new Date());
		smsLogBean.setLogTujuan(noHandphone);
		smsLogBean.setLogUser(authentication.getName());
		HttpRestResponse responseSmsLog = wsBody(wmsParentalWrapper + "/shared/save/sms-log", smsLogBean,
				HttpMethod.POST, headerMap);
		return map;
	}
	
	
	@GetMapping("/get-otp-transaksiEarlyRedemption")
	private Map<String, Object> getcallYouOtptransaksiEarlyRedemption(Authentication authentication, HttpSession session) {
		if(isUnableToRequest(session, "transaksiEarlyRedemption",
				OTP_START_TIME_EARLY, 
				OTP_TIMER_EARLY, OTP_WRONG_ANSWER_EARLY))
		{
			Map<String, Object> map = new HashMap<>();
			map.put("valid", true);
			map.put("message", "Mohon Maaf Anda Belum Dapat Meminta Kode OTP Saat Ini.");
			return map;
		}
		
		String otp = getRandomIntegerBetweenRange(0, 9, 6);

		// TblUserEsbn tblUserEsbn =
		// userAuthenticationCustomLogin.findUserBySid(authentication.getName());

		TokenWrapper tokenWrapper = getMyToken(authentication);
		Map<String, String> headerMap = new HashMap<>();
		headerMap.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());

		String noHandphone = getTblUser(authentication).getUserPhone();

		headerMap.put("secret-phone", noHandphone);
		headerMap.put("secret-message", otp + " adalah kode verifikasi Anda. Mohon jaga keamanan data ini.");
		System.out.println("header otp : " + new Gson().toJson(headerMap));
		HttpRestResponse response = wsBody(wmsParentalWrapper + "/otp-please/v2/calling-me", null, HttpMethod.GET,
				headerMap);
		System.err.println(new Gson().toJson(response));
		Map<String, Object> map = new HashMap<>();

		if (response.getBody().toUpperCase().contains("internal server error".toUpperCase())) {
			map.put("valid", true);
			map.put("message", "Terjadi Kesalahan pada internal kami, kode verifikasi tidak dapat kami kirimkan ");
			return map;
		}

//		if(response.getStatus()!=HttpStatus.ACCEPTED) {
//			map.put("valid", true);
//			map.put("reason", "OTP SENT");
//			map.put("message", "Mohon maaf terjadi kesalahan pada internal kami, kami tidak dapat mengirim OTP anda");
//			session.setAttribute(body,otp);
//			return map;
//		}

		String maskingPhonenumber = maskingPhoneNumber(noHandphone);
		System.err.println(response.getBody());

		map.put("valid", true);
		map.put("reason", "OTP SENT");
		map.put("message", "Kode OTP telah kami kirimkan ke nomor " + maskingPhonenumber + ".\n"
				+ "Anda dapat memperbaharui data Anda pada menu REGISTRASI jika sudah tidak menggunakan nomor handphone tersebut");
		session.setAttribute("transaksiEarlyRedemption", otp);
		session.setAttribute(OTP_WRONG_ANSWER_EARLY, 3);
		session.setAttribute(OTP_TIMER_EARLY, 60);
		
		
		Long startOtpMilis = System.currentTimeMillis();
		session.setAttribute(OTP_START_TIME_EARLY, startOtpMilis);

		TblSmsLogBean smsLogBean = new TblSmsLogBean();
		smsLogBean.setLogPesan(otp + " adalah kode verifikasi Anda. Mohon jaga keamanan data ini.");
		smsLogBean.setLogResend(" ");
		smsLogBean.setLogSubject("OTP Verifikasi");
		smsLogBean.setLogTglWaktu(new Date());
		smsLogBean.setLogTujuan(noHandphone);
		smsLogBean.setLogUser(authentication.getName());
		HttpRestResponse responseSmsLog = wsBody(wmsParentalWrapper + "/shared/save/sms-log", smsLogBean,
				HttpMethod.POST, headerMap);
		return map;
	}
	

	
	@GetMapping("/get-otp-addRekDana")
	private Map<String, Object> getcallYouOtpAddRekDana(Authentication authentication, HttpSession session) {
		
		if(isUnableToRequest(session, "addRekeningDana", "otp_start_addrekdana", 
				"otp_timer_addrekdana", "otp_falied_addrekdana"))
		{
			Map<String, Object> map = new HashMap<>();
			map.put("valid", true);
			map.put("message", "Mohon Maaf Anda Belum Dapat Meminta Kode OTP Saat Ini.");
			return map;
		}
		
		String otp = getRandomIntegerBetweenRange(0, 9, 6);

		// TblUserEsbn tblUserEsbn =
		// userAuthenticationCustomLogin.findUserBySid(authentication.getName());

		TokenWrapper tokenWrapper = getMyToken(authentication);
		Map<String, String> headerMap = new HashMap<>();
		headerMap.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());

		String noHandphone = getTblUser(authentication).getUserPhone();

		headerMap.put("secret-phone", noHandphone);
		headerMap.put("secret-message", otp + " adalah kode verifikasi Anda. Mohon jaga keamanan data ini.");
		System.out.println("header otp : " + new Gson().toJson(headerMap));
		HttpRestResponse response = wsBody(wmsParentalWrapper + "/otp-please/v2/calling-me", null, HttpMethod.GET,
				headerMap);
		System.err.println(new Gson().toJson(response));
		Map<String, Object> map = new HashMap<>();

		if (response.getBody().toUpperCase().contains("internal server error".toUpperCase())) {
			map.put("valid", true);
			map.put("message", "Terjadi Kesalahan pada internal kami, kode verifikasi tidak dapat kami kirimkan ");
			return map;
		}

//		if(response.getStatus()!=HttpStatus.ACCEPTED) {
//			map.put("valid", true);
//			map.put("reason", "OTP SENT");
//			map.put("message", "Mohon maaf terjadi kesalahan pada internal kami, kami tidak dapat mengirim OTP anda");
//			session.setAttribute(body,otp);
//			return map;
//		}

		String maskingPhonenumber = maskingPhoneNumber(noHandphone);
		System.err.println(response.getBody());

		map.put("valid", true);
		map.put("reason", "OTP SENT");
		map.put("message", "Kode OTP telah kami kirimkan ke nomor " + maskingPhonenumber + ".\n"
				+ "Anda dapat memperbaharui data Anda pada menu REGISTRASI jika sudah tidak menggunakan nomor handphone tersebut");
		
		session.setAttribute("addRekeningDana", otp);
		session.setAttribute("otp_falied_addrekdana", 3);
		session.setAttribute("otp_timer_addrekdana", 60);
		Long startOtpMilis = System.currentTimeMillis();
		session.setAttribute("otp_start_addrekdana", startOtpMilis);
		session.setAttribute("otp_check_addrekdana", "addRekeningDana");

		TblSmsLogBean smsLogBean = new TblSmsLogBean();
		smsLogBean.setLogPesan(otp + " adalah kode verifikasi Anda. Mohon jaga keamanan data ini.");
		smsLogBean.setLogResend(" ");
		smsLogBean.setLogSubject("OTP Verifikasi");
		smsLogBean.setLogTglWaktu(new Date());
		smsLogBean.setLogTujuan(noHandphone);
		smsLogBean.setLogUser(authentication.getName());
		HttpRestResponse responseSmsLog = wsBody(wmsParentalWrapper + "/shared/save/sms-log", smsLogBean,
				HttpMethod.POST, headerMap);
		return map;
	}

	@GetMapping("/get-otp-registrationEsbn/{no-phone}")
	private Map<String, Object> getCallYouOtpWithPhoneRegistrationEsbn(
			@PathVariable(value = "no-phone", required = true) String noPhone, Authentication authentication,
			HttpSession session) {
		
		if(isUnableToRequest(session, "registrasiInvestor", "otp_start_regesbn", 
				"otp_timer_regesbn", "otp_failed_regesbn"))
		{
			Map<String, Object> map = new HashMap<>();
			map.put("valid", true);
			map.put("message", "Mohon Maaf Anda Belum Dapat Meminta Kode OTP Saat Ini.");
			return map;
		}
		
		String otp = getRandomIntegerBetweenRange(0, 9, 6);
		TokenWrapper tokenWrapper = getMyToken(authentication);
		Map<String, String> headerMap = new HashMap<>();
		headerMap.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		String noHandphone = noPhone;
		headerMap.put("secret-phone", noHandphone);
		headerMap.put("secret-message", otp + " adalah kode verifikasi Anda. Mohon jaga keamanan data ini.");
		System.out.println("header otp : " + new Gson().toJson(headerMap));
		HttpRestResponse response = wsBody(wmsParentalWrapper + "/otp-please/v2/calling-me", null, HttpMethod.GET,
				headerMap);
		System.err.println(new Gson().toJson(response));
		Map<String, Object> map = new HashMap<>();

		if (response.getBody().toUpperCase().contains("internal server error".toUpperCase())) {
			map.put("valid", true);
			map.put("message", "Terjadi Kesalahan pada internal kami, kode verifikasi tidak dapat kami kirimkan ");
			return map;
		}

		String maskingPhonenumber = maskingPhoneNumber(noHandphone);
		System.err.println(response.getBody());

		map.put("valid", true);
		map.put("reason", "OTP SENT");
		map.put("message", "Kode OTP telah kami kirimkan ke nomor " + maskingPhonenumber + ".\n"
				+ "Anda dapat memperbaharui data Anda pada menu REGISTRASI jika sudah tidak menggunakan nomor handphone tersebut");

		session.setAttribute("registrasiInvestor", otp);
		session.setAttribute("otp_failed_regesbn", 3);
		session.setAttribute("otp_timer_regesbn", 60);
		session.setAttribute("otp_check_regesbn", "registrasiInvestor");
		Long startOtpMilis = System.currentTimeMillis();
		session.setAttribute("otp_start_regesbn", startOtpMilis);
		
		
		TblSmsLogBean smsLogBean = new TblSmsLogBean();
		smsLogBean.setLogPesan(otp + " adalah kode verifikasi Anda. Mohon jaga keamanan data ini.");
		smsLogBean.setLogResend(" ");
		smsLogBean.setLogSubject("OTP Verifikasi");
		smsLogBean.setLogTglWaktu(new Date());
		smsLogBean.setLogTujuan(noHandphone);
		smsLogBean.setLogUser(authentication.getName());
		HttpRestResponse responseSmsLog = wsBody(wmsParentalWrapper + "/shared/save/sms-log", smsLogBean,
				HttpMethod.POST, headerMap);
		return map;
	}
	
	
	
	private String maskingPhoneNumber(String phone) {
		String newPhone = "";
	    char[] h = phone.toCharArray();
	    int i = 1;
	    for (char c : h) {
	    	
			if(i>=4&&i<=8) {
				newPhone += "x";
			}
			else {
				newPhone += Character.toString(c);
			}
	    	i++;
		}
		return newPhone;
	}
}
