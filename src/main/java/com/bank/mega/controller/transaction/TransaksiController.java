package com.bank.mega.controller.transaction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
//import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bank.mega.service.BaseService;

//import com.bank.mega.model.Customers;

@Controller
public class TransaksiController extends BaseService {
	@RequestMapping("/transaksi-pemesanan")
	public String transaksiPemesananCaller(Authentication authentication, Model model, HttpServletRequest request,
			HttpServletResponse response,HttpSession session) {
		informationHeader(model, authentication);
		if (isMenuAccessable(authentication)) {
			session.removeAttribute(OTP_WRONG_ANSWER_PEMESANAN);
			session.removeAttribute(OTP_START_TIME_PEMESANAN);
			session.removeAttribute(OTP_TIMER_PEMESANAN);
			session.removeAttribute("transaksiPemesanan");
			return checkSessionValidity("transaction/transactionPemesanan", authentication, request, response);
		} else {
			return checkSessionValidity("transaction/noreg/transactionPemesanan-noreg", authentication, request, response);
		}

	}

	@RequestMapping("/transaksi-earlyRedemption")
	public String transaksiEarlyRedemptionCaller(Authentication authentication, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		informationHeader(model, authentication);
		if (isMenuAccessable(authentication)) {
			session.removeAttribute(OTP_WRONG_ANSWER_EARLY);
			session.removeAttribute(OTP_START_TIME_EARLY);
			session.removeAttribute(OTP_TIMER_EARLY);
			session.removeAttribute("transaksiEarlyRedemption");
			return checkSessionValidity("transaction/transactionEarlyredemption", authentication, request, response);
		}else {
			return checkSessionValidity("transaction/noreg/transactionEarlyredemption-noreg", authentication, request, response);
			
		}
	}

}
