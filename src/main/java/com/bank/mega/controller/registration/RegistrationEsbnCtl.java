package com.bank.mega.controller.registration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpMethod;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bank.mega.bean.EsbnRegistrationBean;
import com.bank.mega.bean.RekeningDana;
import com.bank.mega.bean.auth.TokenWrapper;
import com.bank.mega.model.TblUserEsbn;
import com.bank.mega.security.HttpRestResponse;
import com.bank.mega.service.BaseService;
import com.google.gson.Gson;

@Controller
public class RegistrationEsbnCtl extends BaseService {

	@RequestMapping("/esbn-registration")
	public String index(Model model, Authentication auth, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws ParseException {
		model.addAttribute("beanRegistry", getWmsValueBySid(auth));
		informationHeader(model, auth);
		session.removeAttribute("otp_failed_regesbn");
		session.removeAttribute("otp_timer_regesbn");
		session.removeAttribute("otp_start_regesbn");
		session.removeAttribute("otp_falied_addrekdana");
		session.removeAttribute("otp_timer_addrekdana");
		session.removeAttribute("otp_start_addrekdana");
		session.removeAttribute("otp_check_addrekdana");
		session.removeAttribute("otp_check_regesbn");
		session.removeAttribute("addRekeningDana");
		session.removeAttribute("registrasiInvestor");
		
		return checkSessionValidity("registration/esbn-registration", auth, request, response);
	}

	@RequestMapping("/ganti-password")
	public String changePassword(Model model, Authentication auth, HttpServletRequest request,
			HttpServletResponse response) {
		informationHeader(model, auth);
		return checkSessionValidity("registration/change-password", auth, request, response);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public EsbnRegistrationBean getWmsValueBySid(Authentication auth) throws ParseException {
		EsbnRegistrationBean bean = new EsbnRegistrationBean();
		TokenWrapper tokenWrapper = getMyToken(auth);
		Map<String, Object> mapingktp = new HashMap<>();
		Map<String, String> headerKTP = new HashMap<>();
		Map<String, Object> mapp = new HashMap<>();
		Map<String, Object> mappp = new HashMap<>();
		Map<String, Object> mapper = new HashMap<>();
		TblUserEsbn tblUserEsbn = getTblUser(auth);
		// tblUserEsbn.setIsFirstEsbnRegist(false);

		HttpRestResponse wsResponse = new HttpRestResponse();
		wsResponse = wsBodyWms("/Inquiry.svc/GetCustomer/" + auth.getName(), "GET", null, null,
				tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		mapp = mapperJsonToHashMap(wsResponse.getBody());
		mappp = mapperJsonToHashMap(new Gson().toJson(mapp.get("Result")));

		if (tblUserEsbn.getIsFirstEsbnRegist() == true) {
			// formated date tgl lahir
			String tglLahir = (String) mappp.get("TanggalLahir");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat output = new SimpleDateFormat("dd/MM/yyyy");
			java.util.Date d = sdf.parse(tglLahir);
			String formattedDate = output.format(d);

			bean.setAlamat((String) mappp.get("Alamat"));
			bean.setSid((String) mappp.get("SID"));
			// bean.setSid("sid44");
			bean.setNamaNasabah((String) mappp.get("Nama"));

			bean.setTempatLahir((String) mappp.get("TempatLahir"));
			bean.setTanggalLahir(formattedDate);
			bean.setJenisKelamin((String) mappp.get("JenisKelamin"));
			bean.setJenisPekerjaan((String) mappp.get("Pekerjaan"));
			bean.setProvinsi((String) mappp.get("Propinsi"));
			bean.setKota((String) mappp.get("Kota"));
			bean.setEmail((String) mappp.get("Email"));
			bean.setKodeJenisKelamin((String) mappp.get("KodeJenisKelamin"));
			bean.setKodeKota((String) mappp.get("KodeKota"));
			bean.setKodePekerjaan((String) mappp.get("KodePekerjaan"));

			String nohp = null;
			String notlp = null;
			notlp = (String) mappp.get("NoTelepon");
			nohp = (String) mappp.get("NoHandphone");
			if (nohp != null) {
				bean.setNoHandphone(nohp);
			} else {
				nohp = "";
				bean.setNoHandphone(nohp);
			}

			if (notlp != null) {
				bean.setNoTelepon(notlp);
			} else {
				notlp = "";
				bean.setNoTelepon(notlp);
			}

			String noRekeningDana = null;
			String idBank = null;
			List<Map<String, String>> listRekening = (List<Map<String, String>>) mappp.get("RekeningDana");
			List<RekeningDana> listRekDana = new ArrayList<RekeningDana>();
			try {
				for (Map mapRek : listRekening) {
					RekeningDana dana = new RekeningDana();
					noRekeningDana = (String) mapRek.get("NoRekening");
					idBank = (String) mapRek.get("IdBank");
					dana.setNoRekening(noRekeningDana);
					dana.setIdBank(idBank);
					listRekDana.add(dana);
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			String noRekeningSurat = null;
			String namaSubregistry = null;
			String idPartisipan = null;
			String idSubregistry = null;
			String namaPartisipan = null;
			List<Map<String, String>> listRekeningSurat = (List<Map<String, String>>) mappp
					.get("RekeningSuratBerharga");

			if (listRekeningSurat.isEmpty() || "[]".equals(listRekeningSurat) || null == listRekeningSurat) {
				noRekeningSurat = "";
				namaSubregistry = "";
				idPartisipan = "";
				idSubregistry = "";
				namaPartisipan = "";

			} else {
				noRekeningSurat = listRekeningSurat.get(0).get("NoRekening");
				namaSubregistry = listRekeningSurat.get(0).get("NamaSubRegistry");
				idPartisipan = listRekeningSurat.get(0).get("IdPartisipan");
				idSubregistry = listRekeningSurat.get(0).get("IdSubregistry");
				namaPartisipan = listRekeningSurat.get(0).get("NamaPartisipan");
			}
			bean.setIdPartisipan(idPartisipan);
			bean.setIdSubregistry(idSubregistry);
			bean.setNamaPartisipan(namaPartisipan);
			bean.setNamaSubregistry(namaSubregistry);
			bean.setNoRekSuratBerharga(noRekeningSurat);
			bean.setNoRekeningDana(listRekDana);

		} else {
			Map<String, String> headerMap = new HashMap<>();

			headerMap.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
			// headerMap.put("secret-url", "/v1/investor/" + "sid48");
			headerMap.put("secret-url", "/v1/investor/" + auth.getName().toUpperCase());
			headerMap.put("secret-method", "GET");
			wsResponse = wsBody(wmsParentalWrapper + "esbn-connector/call-url/no-body", null, HttpMethod.GET,
					headerMap);

			mapper = mapperJsonToHashMap(wsResponse.getBody());

			// formated date tgl lahir
			String tglLahir = (String) mapper.get("TglLahir");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			SimpleDateFormat output = new SimpleDateFormat("dd/MM/yyyy");
			java.util.Date d = sdf.parse(tglLahir);
			String formattedDate = output.format(d);

			bean.setAlamat((String) mapper.get("Alamat"));
			bean.setSid((String) mapper.get("Sid"));
			// bean.setSid("sid44");
			bean.setNamaNasabah((String) mapper.get("Nama"));

			bean.setTempatLahir((String) mapper.get("TempatLahir"));
			bean.setTanggalLahir(formattedDate);
			bean.setJenisKelamin((String) mapper.get("JenisKelamin"));
			bean.setJenisPekerjaan((String) mapper.get("Pekerjaan"));
			bean.setProvinsi((String) mapper.get("Provinsi"));
			bean.setKota((String) mapper.get("Kota"));
			bean.setEmail(tblUserEsbn.getUserEmail());
			bean.setKodeJenisKelamin((String) mapper.get("KdJenisKelamin"));
			bean.setKodeKota((String) mapper.get("KdKota"));
			bean.setKodePekerjaan((String) mapper.get("KdPekerjaan"));

			String nohp = null;
			String notlp = null;
			notlp = (String) mapper.get("NoTelp");
			nohp = tblUserEsbn.getUserPhone();
			if (nohp != null) {
				bean.setNoHandphone(nohp);
			} else {
				nohp = "";
				bean.setNoHandphone(nohp);
			}

			if (notlp != null) {
				bean.setNoTelepon(notlp);
			} else {
				notlp = "";
				bean.setNoTelepon(notlp);
			}

			String noRekeningDana = null;
			Integer idBank = null;
			List<Map<String, String>> listRekening = (List<Map<String, String>>) mapper.get("RekeningDana");
			List<RekeningDana> listRekDana = new ArrayList<RekeningDana>();
			try {
				for (Map mapRek : listRekening) {
					RekeningDana dana = new RekeningDana();
					noRekeningDana = (String) mapRek.get("NoRek");
					idBank = (Integer) mapRek.get("IdBank");
					dana.setNoRekening(noRekeningDana);
					dana.setIdBank(idBank.toString());
					listRekDana.add(dana);
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			String noRekeningSurat = null;
			String namaSubregistry = null;
			String idPartisipan = null;
			String idSubregistry = null;
			String namaPartisipan = null;
			List<Map<String, String>> listRekeningSurat = (List<Map<String, String>>) mapper.get("RekeningSB");

			if (listRekeningSurat.isEmpty() || "[]".equals(listRekeningSurat) || null == listRekeningSurat) {
				noRekeningSurat = "";
				namaSubregistry = "";
				idPartisipan = "";
				idSubregistry = "";
				namaPartisipan = "";

			} else {
				noRekeningSurat = listRekeningSurat.get(0).get("NoRek");
				namaSubregistry = listRekeningSurat.get(0).get("NamaSubregistry");
				idPartisipan = String.valueOf(listRekeningSurat.get(0).get("IdPartisipan"));
				idSubregistry = String.valueOf(listRekeningSurat.get(0).get("IdSubregistry"));
				namaPartisipan = listRekeningSurat.get(0).get("NamaPartisipan");
			}
			bean.setIdPartisipan(idPartisipan.toString());
			bean.setIdSubregistry(idSubregistry.toString());
			bean.setNamaPartisipan(namaPartisipan);
			bean.setNamaSubregistry(namaSubregistry);
			bean.setNoRekSuratBerharga(noRekeningSurat);
			bean.setNoRekeningDana(listRekDana);

		}
		headerKTP.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		headerKTP.put("secret-field", "EKTP");
		headerKTP.put("secret-sid", auth.getName());
		HttpRestResponse resultKtp = wsBody(wmsParentalWrapper + "/registeration/GetOneDetailUser", null,
				HttpMethod.GET, headerKTP);

		mapingktp = mapperJsonToHashMap(resultKtp.getBody());
		bean.setNoKtp((String) mapingktp.get("itemDescription"));

		return bean;
	}
}
