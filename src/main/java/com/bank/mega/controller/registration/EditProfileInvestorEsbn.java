package com.bank.mega.controller.registration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bank.mega.bean.ProfileInvestor;
import com.bank.mega.bean.RekeningDana;
import com.bank.mega.bean.auth.TokenWrapper;
import com.bank.mega.security.HttpRestResponse;
import com.bank.mega.service.BaseService;

@Controller
public class EditProfileInvestorEsbn extends BaseService {
	@RequestMapping("/esbn-editProfile")
	public String index(Model model, Authentication auth) throws ParseException {
		model.addAttribute("esbnProfiel", getInvestorBySid(auth));
		informationHeader(model, auth);
		return "registration/esbn-edit-profile";
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ProfileInvestor getInvestorBySid(Authentication auth) throws ParseException {
		ProfileInvestor investor = new ProfileInvestor();

		TokenWrapper tokenWrapper = getMyToken(auth);
		Map<String, String> headerMap = new HashMap<>();

		headerMap.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		headerMap.put("secret-url", "/v1/investor/" + "sid29");
		headerMap.put("secret-method", "GET");
		HttpRestResponse result = wsBody(wmsParentalWrapper + "esbn-connector/call-url/no-body", null, HttpMethod.GET,
				headerMap);
		Map<String, Object> mapBody = mapperJsonToHashMap(result.getBody());

		investor.setAlamat((String) mapBody.get("Alamat"));
		investor.setSid((String) mapBody.get("Sid"));
		investor.setNama((String) mapBody.get("Nama"));
		investor.setNoIdentitas((String) mapBody.get("NoIdentitas"));
		investor.setTempatLahir((String) mapBody.get("TempatLahir"));

		// formated date tgl lahir
		String tglLahir = (String) mapBody.get("TglLahir");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date d = sdf.parse(tglLahir);
		String formattedDate = output.format(d);

		investor.setTglLahir(formattedDate);
		investor.setJenisKelamin((String) mapBody.get("JenisKelamin"));
		investor.setPekerjaan((String) mapBody.get("Pekerjaan"));
		investor.setProvinsi((String) mapBody.get("Provinsi"));
		investor.setKota((String) mapBody.get("Kota"));

		investor.setNoHp((String) mapBody.get("NoHp"));
		investor.setEmail((String) mapBody.get("Email"));

		String noRekeningDana = null;
		String noRekeningSurat = null;
		String namaSubregistry = null;
		String namaBank = null;
		String notlp = null;

		notlp = (String) mapBody.get("NoTelp");

		if (notlp != null) {
			investor.setNoTelp(notlp);
		} else {
			notlp = "";
			investor.setNoTelp(notlp);
		}

		List<Map<String, String>> listRekening = (List<Map<String, String>>) mapBody.get("RekeningDana");

		List<RekeningDana> listRekDana = new ArrayList<RekeningDana>();
		for (Map mapRek : listRekening) {
			RekeningDana dana = new RekeningDana();
			noRekeningDana = (String) mapRek.get("NoRek");
			namaBank = (String) mapRek.get("NamaBank");
			dana.setNoRekening(noRekeningDana);
			dana.setBank(namaBank);
			listRekDana.add(dana);
		}
		List<Map<String, String>> listRekeningSurat = (List<Map<String, String>>) mapBody.get("RekeningSB");

		if (listRekeningSurat.isEmpty() || "[]".equals(listRekeningSurat) || null == listRekeningSurat) {
			noRekeningSurat = "";
			namaSubregistry = "";

		} else {
			noRekeningSurat = listRekeningSurat.get(0).get("NoRek");
			namaSubregistry = listRekeningSurat.get(0).get("NamaSubregistry");
		}
		investor.setNamaSubregistry(namaSubregistry);
		investor.setRekeningSB(noRekeningSurat);
		investor.setRekeningDana(listRekDana);
		return investor;
	}
}
