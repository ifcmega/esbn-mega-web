package com.bank.mega.controller.registration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.http.HttpMethod;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bank.mega.bean.ErrorHttpHandler;
import com.bank.mega.bean.EsbnInfoUser;
import com.bank.mega.bean.EsbnRegistrationBean;
import com.bank.mega.bean.EsbnRekeningDana;
import com.bank.mega.bean.ProfileInvestor;
import com.bank.mega.bean.RekeningDana;
import com.bank.mega.bean.RekeningDanaDummy;
import com.bank.mega.bean.RekeningDanaInvestor;
import com.bank.mega.bean.RekeningSuratBerharga;
import com.bank.mega.bean.ReqRekDana;
import com.bank.mega.bean.ReqRekSb;
import com.bank.mega.bean.RequestBodyRegistrationEsbn;
import com.bank.mega.bean.SaveInvestorBean;
import com.bank.mega.bean.auth.TokenWrapper;
import com.bank.mega.model.TblUserEsbn;
import com.bank.mega.security.HttpRestResponse;
import com.bank.mega.security.WsResponse;
import com.bank.mega.service.BaseService;
import com.bank.mega.validator.EmailValidator;
import com.bank.mega.validator.PhoneValidator;
import com.google.gson.Gson;

@RestController
@RequestMapping("/registrationWsCtl")
public class RegistrationEsbnWsCtl extends BaseService {

	private String generateToken(Authentication authentication) {
		TokenWrapper tokenWrapper = getMyToken(authentication);
		return tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token();
	}

	private String maskingPhoneNumber(String phone) {
		String newPhone = "";
		StringBuilder sb = new StringBuilder(phone);
		int ini = phone.length() - 4;
		for (int i = 0; i < ini; ++i) {
			sb.setCharAt(i, '*');
		}
		newPhone = sb.toString();
		return newPhone;
	}

	private String maskingEmail(String email) {
		String[] part = email.split("@");
		String newEmail = null;
		StringBuilder sb = new StringBuilder(email);
		int ini = part[0].length() - 4;
		for (int i = 0; i < ini && sb.charAt(i) != '@'; ++i) {
			sb.setCharAt(i, '*');
		}
		newEmail = sb.toString();
		return newEmail;
	}

	@GetMapping("/count-otp-reg-esbn")
	public Map<String, Object> countOtpRegEsbn(HttpSession session) {
		Map<String, Object> map = new HashMap<>();
		map.put("valid", false);
		if (session.getAttribute("otp_timer_regesbn") != null) {
			int counter = (int) session.getAttribute("otp_timer_regesbn") - 1;
			session.setAttribute("otp_timer_regesbn", counter);
			map.put("valid", true);
			map.put("value", counter);
		}

		return map;
	}

	@GetMapping("/count-otp-add-rekdana")
	public Map<String, Object> countOtpAddRekDana(HttpSession session) {
		Map<String, Object> map = new HashMap<>();
		map.put("valid", false);
		if (session.getAttribute("otp_timer_addrekdana") != null) {
			int counter = (int) session.getAttribute("otp_timer_addrekdana") - 1;
			session.setAttribute("otp_timer_addrekdana", counter);
			map.put("valid", true);
			map.put("value", counter);
		}

		return map;
	}

	/*
	 * @GetMapping("/otp-count") public Map<String, Object> countOtp(HttpSession
	 * session) { Map<String, Object> map = new HashMap<>(); map.put("valid",
	 * false); if (session.getAttribute(OTP_TIMER) != null ||
	 * session.getAttribute(OTP_TIMER) != "") { int count = (int)
	 * session.getAttribute(OTP_TIMER) - 1; session.setAttribute(OTP_TIMER, count);
	 * map.put("valid", true); map.put("value", count); }
	 * 
	 * return map; }
	 */

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping("/getRekDana")
	public List<RekeningDana> getRekeningDana(Authentication auth) {
		TokenWrapper tokenWrapper = getMyToken(auth);

		HttpRestResponse wsResponse = wsBodyWms("/Inquiry.svc/GetCustomer/" + auth.getName().toUpperCase(), "GET", null,
				null, tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());

		Map<String, Object> mapp = mapperJsonToHashMap(wsResponse.getBody());
		Map<String, Object> mappp = mapperJsonToHashMap(new Gson().toJson(mapp.get("Result")));

		String noRekeningDana = null;
		String idBank = null;
		List<Map<String, String>> listRekening = (List<Map<String, String>>) mappp.get("RekeningDana");
		List<RekeningDana> listRekDana = new ArrayList<RekeningDana>();
		/*
		 * RekeningDana dana2 = new RekeningDana(); dana2.setNoRekening("112234000000");
		 * dana2.setIdBank("122"); listRekDana.add(dana2);
		 */
		for (Map mapRek : listRekening) {
			RekeningDana dana = new RekeningDana();

			noRekeningDana = (String) mapRek.get("NoRekening");
			idBank = (String) mapRek.get("IdBank");

			dana.setNoRekening(noRekeningDana);
			dana.setIdBank(idBank);
			dana.setIdBankNorek(noRekeningDana + "||" + idBank);
			listRekDana.add(dana);
		}

		return listRekDana;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping("/getRekSB")
	public List<RekeningSuratBerharga> getRekeningSB(Authentication auth) {
		TokenWrapper tokenWrapper = getMyToken(auth);

		HttpRestResponse wsResponse = wsBodyWms("/Inquiry.svc/GetCustomer/" + auth.getName().toUpperCase(), "GET", null,
				null, tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());

		Map<String, Object> mapp = mapperJsonToHashMap(wsResponse.getBody());
		Map<String, Object> mappp = mapperJsonToHashMap(new Gson().toJson(mapp.get("Result")));

		String noRekeningDana = null;
		String idPartisipan = null;
		String idSubregistry = null;
		List<Map<String, String>> listRekening = (List<Map<String, String>>) mappp.get("RekeningSuratBerharga");
		List<RekeningSuratBerharga> listRekSB = new ArrayList<RekeningSuratBerharga>();
		/*
		 * RekeningDana dana2 = new RekeningDana(); dana2.setNoRekening("112234000000");
		 * dana2.setIdBank("122"); listRekDana.add(dana2);
		 */
		for (Map mapRek : listRekening) {
			RekeningSuratBerharga sb = new RekeningSuratBerharga();

			noRekeningDana = (String) mapRek.get("NoRekening");
			idPartisipan = (String) mapRek.get("IdPartisipan");
			idSubregistry = (String) mapRek.get("IdSubregistry");

			sb.setNoRekening(noRekeningDana);
			sb.setIdPartisipan(idPartisipan);
			sb.setIdSubregistry(idSubregistry);
			listRekSB.add(sb);
		}

		return listRekSB;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping("/getRekSB/unExisting")
	public List<RekeningSuratBerharga> getRekeningSBUnExisting(Authentication auth) {
		TokenWrapper tokenWrapper = getMyToken(auth);

		HttpRestResponse wsResponse = wsBodyWms("/Inquiry.svc/GetCustomer/" + auth.getName().toUpperCase(), "GET", null,
				null, tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());

		/*
		 * HttpRestResponse wsRespons2e = wsBodyEsbnWithoutBody("/v1/rekeningsb/" +
		 * auth.getName().toUpperCase(), "GET", null, generateToken(auth));
		 */

		Map<String, Object> mapp = mapperJsonToHashMap(wsResponse.getBody());
		Map<String, Object> mappp = mapperJsonToHashMap(new Gson().toJson(mapp.get("Result")));

		String noRekeningDana = null;
		String idPartisipan = null;
		String idSubregistry = null;
		List<Map<String, String>> listRekening = (List<Map<String, String>>) mappp.get("RekeningSuratBerharga");
		List<RekeningSuratBerharga> listRekSB = new ArrayList<RekeningSuratBerharga>();
		/*
		 * RekeningDana dana2 = new RekeningDana(); dana2.setNoRekening("112234000000");
		 * dana2.setIdBank("122"); listRekDana.add(dana2);
		 */
		for (Map mapRek : listRekening) {
			RekeningSuratBerharga sb = new RekeningSuratBerharga();

			noRekeningDana = (String) mapRek.get("NoRekening");
			idPartisipan = (String) mapRek.get("IdPartisipan");
			idSubregistry = (String) mapRek.get("IdSubregistry");

			sb.setNoRekening(noRekeningDana);
			sb.setIdPartisipan(idPartisipan);
			sb.setIdSubregistry(idSubregistry);
			listRekSB.add(sb);
		}

		return listRekSB;
	}

	@SuppressWarnings("unused")
	@GetMapping("/check-existing-rekeningdana/{noRek}")
	public Map<String, Object> checkExistencyRekeningDana(@PathVariable(name = "noRek") String noRek,
			Authentication auth) {
		Map<String, Object> map = new HashMap<>();
		boolean valid = false;
		String reason = "";

		TokenWrapper tokenWrapper = getMyToken(auth);

		HttpRestResponse wsResponse = wsBodyWms("/Inquiry.svc/GetCustomer/" + auth.getName().toUpperCase(), "GET", null,
				null, tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());

		map.put("reason", reason);
		map.put("valid", valid);
		return map;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping("/getRekDana/unExisting")
	public List<RekeningDana> getRekeningDanaUnexisting(Authentication auth) {
		TokenWrapper tokenWrapper = getMyToken(auth);

		HttpRestResponse wsResponse = wsBodyWms("/Inquiry.svc/GetCustomer/" + auth.getName().toUpperCase(), "GET", null,
				null, tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		HttpRestResponse wsRespons2e = wsBodyEsbnWithoutBody("/v1/investor/" + auth.getName().toUpperCase(), "GET",
				null, generateToken(auth));
		EsbnInfoUser infoUser = new EsbnInfoUser();
		try {
			infoUser = mapperJsonToSingleDto(wsRespons2e.getBody(), EsbnInfoUser.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Map<String, Object> mapp = mapperJsonToHashMap(wsResponse.getBody());
		Map<String, Object> mappp = mapperJsonToHashMap(new Gson().toJson(mapp.get("Result")));

		String noRekeningDana = null;
		String idBank = null;
		List<Map<String, String>> listRekening = (List<Map<String, String>>) mappp.get("RekeningDana");
		List<RekeningDana> listRekDana = new ArrayList<RekeningDana>();
		List<String> rdns = new ArrayList<>();
		for (EsbnRekeningDana rd : infoUser.getRekeningDana()) {
			rdns.add(rd.getNoRek().trim());
		}
		for (Map mapRek : listRekening) {
			RekeningDana dana = new RekeningDana();

			noRekeningDana = (String) mapRek.get("NoRekening");
			idBank = (String) mapRek.get("IdBank");

			dana.setNoRekening(noRekeningDana);
			dana.setIdBank(idBank);
			dana.setIdBankNorek(noRekeningDana + "||" + idBank);

			if (!rdns.contains(noRekeningDana.trim())) {
				// System.err.println("bandingkan setelah if: " +rd.getNoRek() + " " +
				// noRekeningDana);
				listRekDana.add(dana);
			}
		}

		return listRekDana;
	}

	@SuppressWarnings("unused")
	@PostMapping("/save-myRegistrationEsbn")
	public Map<String, Object> registerSidToEsbnWebNew(@RequestBody RequestBodyRegistrationEsbn request,
			Authentication auth, HttpSession session) throws ParseException {
		Map<String, Object> mapBody = new HashMap<String, Object>();
		ProfileInvestor bean = new ProfileInvestor();
		TokenWrapper tokenWrapper = getMyToken(auth);
		SaveInvestorBean investor = new SaveInvestorBean();
		ReqRekDana dana = new ReqRekDana();
		TblUserEsbn tblUserEsbn = getTblUser(auth);
		Map<String, String> headerMapUpdatePhone = new HashMap<>();
		Map<String, String> headerMapUpdateEmail = new HashMap<>();
		Map<String, Object> errorMapInvestor = new HashMap<>();
		Map<String, Object> errorMapRekeningDana = new HashMap<>();
		Map<String, Object> errorMapRekeningSB = new HashMap<>();
		Map<String, String> headerMapUpdateFirstRegistUpdateInvestor = new HashMap<>();
		Map<String, String> headerMapUpdateFirstRegist = new HashMap<>();

		int timerOtp = (int) session.getAttribute("otp_timer_regesbn");
		int wrongOtpAnswer = (int) session.getAttribute("otp_failed_regesbn");
		long timerOtpStart = (long) session.getAttribute("otp_start_regesbn");
		Float porcessCallWs = (System.currentTimeMillis() - timerOtpStart) / 1000F;
		String otpCheck = (String) session.getAttribute("otp_check_regesbn");
		System.out.println(otpCheck);
		System.out.println("otp wrong : " + new Gson().toJson(wrongOtpAnswer));
		System.out.println("otp timer : " + new Gson().toJson(porcessCallWs));

		if (!"registrasiInvestor".equalsIgnoreCase(otpCheck)) {
			mapBody.put("valid", false);
			mapBody.put("reason", "Mohon minta kode OTP terlebih dahulu");
			return mapBody;
		}

		if (callOtp(session, "registrasiInvestor", "") == null || callOtp(session, "registrasiInvestor", "") == "") {
			mapBody.put("valid", false);
			mapBody.put("reason", "Mohon isi kode OTP terlebih dahulu");
			return mapBody;
		}

		if (timerOtp <= 0 || porcessCallWs >= 60) {
			mapBody.put("valid", false);
			mapBody.put("reason", "Kode OTP yang anda minta sudah kadaluarsa. Mohon minta kembali kode OTP anda");
			return mapBody;
		}

		if (wrongOtpAnswer <= 0) {
			mapBody.put("valid", false);
			mapBody.put("reason", "Anda sudah melakukan 3 kali kesalahan. Mohon minta kembali kode OTP anda");
			session.removeAttribute("otp_timer_regesbn");
			session.removeAttribute("otp_failed_regesbn");
			session.removeAttribute("otp_start_regesbn");
			session.removeAttribute("otp_check_regesbn");
			session.removeAttribute("registrasiInvestor");
			return mapBody;
		}

		if (request.getMyOtp() == null) {
			mapBody.put("valid", false);
			mapBody.put("reason", "Mohon isi kode OTP terlebih dahulu");
			return mapBody;
		}

		if (!request.getMyOtp().equalsIgnoreCase(callOtp(session, "registrasiInvestor", ""))) {
			mapBody.put("valid", false);
			mapBody.put("reason", "Mohon maaf kode OTP yang Anda masukkan belum tepat.");
			session.setAttribute("otp_failed_regesbn", wrongOtpAnswer - 1);
			return mapBody;
		}

		if (!EmailValidator.isValid(request.getEmail())) {
			mapBody.put("valid", false);
			mapBody.put("reason", "Mohon maaf format email yang Anda masukkan belum tepat.");
			return mapBody;
		}

		if (!PhoneValidator.isValid(request.getNoHp())) {
			mapBody.put("valid", false);
			mapBody.put("reason", "Mohon maaf format nomor handphone yang Anda masukkan belum tepat.");
			return mapBody;
		}

		EsbnRegistrationBean registrationBean = getWmsCustomerAndInvestorValueBySid(auth);

		checkRekDana(auth, request);

		String tglLahir = registrationBean.getTanggalLahir();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date d = sdf.parse(tglLahir);
		String formattedDate = output.format(d);

		// body send for regist investor
		investor.setSid(registrationBean.getSid());
		investor.setNama(registrationBean.getNamaNasabah());
		investor.setNoIdentitas(registrationBean.getNoKtp());
		investor.setTempatLahir(registrationBean.getTempatLahir());
		investor.setTglLahir(formattedDate);
		investor.setKdJenisKelamin(registrationBean.getKodeJenisKelamin());
		investor.setKdPekerjaan(registrationBean.getKodePekerjaan());
		investor.setKdKota(registrationBean.getKodeKota());
		investor.setAlamat(registrationBean.getAlamat());
		investor.setNoTelp(registrationBean.getNoTelepon());
		investor.setNoHp(request.getNoHp());
		investor.setEmail(request.getEmail());

		HttpRestResponse resultInvestor = wsBodyEsbnWithBody("/v1/investor", "POST", investor, null,
				generateToken(auth));

		String bodyResultInvestor = resultInvestor.getBody();

		// update phone to database
		headerMapUpdatePhone.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		headerMapUpdatePhone.put("secret-field", "phone");
		headerMapUpdatePhone.put("secret-sid", auth.getName());
		Map<String, Object> mapBodyUpdatePhone = new HashMap<>();
		mapBodyUpdatePhone.put("phone", request.getNoHp());

		WsResponse wsRespPhone = getResultWs(wmsParentalWrapper + "/registeration/update-data-user", mapBodyUpdatePhone,
				HttpMethod.PUT, headerMapUpdatePhone);

		// update email to database
		headerMapUpdateEmail.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		headerMapUpdateEmail.put("secret-field", "email");
		headerMapUpdateEmail.put("secret-sid", auth.getName());
		Map<String, Object> mapBodyUpdateEmail = new HashMap<>();
		mapBodyUpdateEmail.put("email", request.getEmail());

		WsResponse wsRespEmail = getResultWs(wmsParentalWrapper + "/registeration/update-data-user", mapBodyUpdateEmail,
				HttpMethod.PUT, headerMapUpdateEmail);

		try {
			ErrorHttpHandler errorHandlerInvestor = mapperJsonToSingleDto(bodyResultInvestor, ErrorHttpHandler.class);
			if (errorHandlerInvestor.getCode() != 0) {
				if (errorHandlerInvestor.getCode() == 101 || errorHandlerInvestor.getCode() == 100) {
					HttpRestResponse resultUpdateInvestor = wsBodyEsbnWithBody(
							"/v1/investor/" + auth.getName().toUpperCase(), "PUT", investor, null, generateToken(auth));
					String bodyResultUpdateInvestor = resultUpdateInvestor.getBody();

					try {
						ErrorHttpHandler errorHandlerUpdateInvestor = mapperJsonToSingleDto(bodyResultUpdateInvestor,
								ErrorHttpHandler.class);
						if (errorHandlerUpdateInvestor.getCode() != 0) {
							if (errorHandlerUpdateInvestor.getCode() == 102
									|| errorHandlerUpdateInvestor.getCode() == 103) {
								errorMapInvestor.put("valid", false);
								errorMapInvestor.put("reason",
										"Terjadi kesalahan di sistem kami. Segera hubungi Cabang Bank Mega terdekat.");
								return errorMapInvestor;
							} else {
								errorMapInvestor.put("valid", false);
								errorMapInvestor.put("reason", errorHandlerUpdateInvestor.getMessage());
								return errorMapInvestor;
							}
						} else {
							errorMapInvestor = mapperJsonToHashMap(bodyResultUpdateInvestor);
							errorMapInvestor.put("reason", "Data berhasil terupdate");
							errorMapInvestor.put("valid", true);

							// send email if first regist
							if (tblUserEsbn.getIsFirstEsbnRegist() == true) {
								sendEmailRegistration(request.getEmail(), request.getNama(), request.getSid(),
										request.getNoRekSurat(), request.getNoRekTerpilih().getNoRekening());

								if (request.getNoRekTerpilih() != null) {
									RekeningDana rekeningDana = request.getNoRekTerpilih();
									dana.setIdBank(rekeningDana.getIdBank());
									dana.setSid(request.getSid());
									dana.setNoRek(rekeningDana.getNoRekening());
									dana.setNama(request.getNama());
								} else {
									dana = new ReqRekDana();
								}

								HttpRestResponse resultAddDana = wsBodyEsbnWithBody("/v1/rekeningdana", "POST", dana,
										null, generateToken(auth));

								ReqRekSb sb = new ReqRekSb();

								sb.setIdPartisipan(registrationBean.getIdPartisipan());
								sb.setIdSubregistry(registrationBean.getIdSubregistry());
								sb.setSid(registrationBean.getSid());
								sb.setNoRek(request.getNoRekSurat());
								sb.setNama(registrationBean.getNamaNasabah());

								HttpRestResponse resultAddSB = wsBodyEsbnWithBody("/v1/rekeningsb", "POST", sb, null,
										generateToken(auth));

							}

							headerMapUpdateFirstRegistUpdateInvestor.put("Authorization",
									tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
							headerMapUpdateFirstRegistUpdateInvestor.put("secret-field", "isFirstRegist");
							headerMapUpdateFirstRegistUpdateInvestor.put("secret-sid", auth.getName());
							Map<String, Object> mapBodyUpdateFirstRegistUpdateInvestor = new HashMap<>();
							mapBodyUpdateFirstRegistUpdateInvestor.put("isFirstRegist", false);

							WsResponse wsRespUpdateFirstRegistUpdateInvestor = getResultWs(
									wmsParentalWrapper + "/registeration/update-data-user",
									mapBodyUpdateFirstRegistUpdateInvestor, HttpMethod.PUT,
									headerMapUpdateFirstRegistUpdateInvestor);

							if ((!request.getOldPhone().equals(request.getNoHp()))
									&& (!request.getOldEmail().equals(request.getEmail()))) {
								sendConfirmChangeEmail(request.getOldEmail(), registrationBean.getNamaNasabah(),
										maskingEmail(request.getEmail()), auth.getName());
								sendConfirmChangePhone(request.getOldEmail(), registrationBean.getNamaNasabah(),
										maskingPhoneNumber(request.getNoHp()), auth.getName());
							} else if (!request.getOldPhone().equals(request.getNoHp())) {
								sendConfirmChangePhone(request.getOldEmail(), registrationBean.getNamaNasabah(),
										maskingPhoneNumber(request.getNoHp()), auth.getName());
							} else if (!request.getOldEmail().equals(request.getEmail())) {
								sendConfirmChangeEmail(request.getOldEmail(), registrationBean.getNamaNasabah(),
										maskingEmail(request.getEmail()), auth.getName());
							} else {
								errorMapInvestor.put("valid", true);
								errorMapInvestor.put("reason", "Data tidak ada yang berubah");
							}

							session.removeAttribute("otp_failed_regesbn");
							session.removeAttribute("otp_timer_regesbn");
							session.removeAttribute("otp_start_regesbn");
							session.removeAttribute("otp_check_regesbn");

							return errorMapInvestor;
						}
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				} else {
					errorMapInvestor.put("valid", false);
					errorMapInvestor.put("reason", errorHandlerInvestor.getMessage());
					return errorMapInvestor;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		// Add Rekening Dana
		if (request.getNoRekTerpilih() != null) {
			RekeningDana rekeningDana = request.getNoRekTerpilih();
			dana.setIdBank(rekeningDana.getIdBank());
			dana.setSid(registrationBean.getSid());
			dana.setNoRek(rekeningDana.getNoRekening());
			dana.setNama(registrationBean.getNamaNasabah());
		} else {
			dana = new ReqRekDana();
		}

		HttpRestResponse resultRekeningDana = wsBodyEsbnWithBody("/v1/rekeningdana", "POST", dana, null,
				generateToken(auth));

		try {
			ErrorHttpHandler errorHandlerRekeningDana = mapperJsonToSingleDto(resultRekeningDana.getBody(),
					ErrorHttpHandler.class);
			if (errorHandlerRekeningDana.getCode() != 0) {
				if (errorHandlerRekeningDana.getCode() == 102 || errorHandlerRekeningDana.getCode() == 203) {
					errorMapRekeningDana.put("valid", false);
					errorMapRekeningDana.put("reason",
							"Terjadi kesalahan di sistem kami. Segera hubungi Cabang Bank Mega terdekat.");
					return errorMapRekeningDana;
				} else {
					errorMapRekeningDana.put("valid", false);
					errorMapRekeningDana.put("reason", errorHandlerRekeningDana.getMessage());
					return errorMapRekeningDana;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		// Add Rekening SB
		ReqRekSb sb = new ReqRekSb();

		sb.setIdPartisipan(request.getIdPartisipan());
		sb.setIdSubregistry(request.getIdSubregistry());
		sb.setSid(registrationBean.getSid());
		sb.setNoRek(request.getNoRekSurat());
		sb.setNama(registrationBean.getNamaNasabah());

		HttpRestResponse resultRekeningSB = wsBodyEsbnWithBody("/v1/rekeningsb", "POST", sb, null, generateToken(auth));

		try {
			ErrorHttpHandler errorHandlerRekeningSB = mapperJsonToSingleDto(resultRekeningSB.getBody(),
					ErrorHttpHandler.class);
			if (errorHandlerRekeningSB.getCode() != 0) {
				if (errorHandlerRekeningSB.getCode() == 102 || errorHandlerRekeningSB.getCode() == 303) {
					errorMapRekeningSB.put("valid", false);
					errorMapRekeningSB.put("reason",
							"Terjadi kesalahan di sistem kami. Segera hubungi Cabang Bank Mega terdekat.");
					return errorMapRekeningSB;
				} else {
					errorMapRekeningSB.put("valid", false);
					errorMapRekeningSB.put("reason", errorHandlerRekeningSB.getMessage());
					return errorMapRekeningSB;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		if (bodyResultInvestor != null) {
			mapBody = mapperJsonToHashMap(bodyResultInvestor);
			// HttpRestResponse result = resultAddNewInvestor(registrasiInvestor, auth);
			bean.setAlamat((String) mapBody.get("Alamat"));
			bean.setSid((String) mapBody.get("Sid"));
			bean.setNama((String) mapBody.get("Nama"));
			bean.setNoIdentitas((String) mapBody.get("NoIdentitas"));
			bean.setTempatLahir((String) mapBody.get("TempatLahir"));
			bean.setTglLahir((String) mapBody.get("TglLahir"));
			bean.setJenisKelamin((String) mapBody.get("JenisKelamin"));
			bean.setPekerjaan((String) mapBody.get("Pekerjaan"));
			bean.setProvinsi((String) mapBody.get("Provinsi"));
			bean.setKota((String) mapBody.get("Kota"));
			bean.setNoTelp((String) mapBody.get("NoTelp"));
			bean.setNoHp((String) mapBody.get("NoHp"));
			bean.setEmail((String) mapBody.get("Email"));

			// send email if first regist
			if (tblUserEsbn.getIsFirstEsbnRegist() == true) {
				sendEmailRegistration(bean.getEmail(), bean.getNama(), bean.getSid(), request.getNoRekSurat(),
						request.getNoRekTerpilih().getNoRekening());
			}

			// update database first register
			headerMapUpdateFirstRegist.put("Authorization",
					tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
			headerMapUpdateFirstRegist.put("secret-field", "isFirstRegist");
			headerMapUpdateFirstRegist.put("secret-sid", auth.getName());
			Map<String, Object> mapBodyFirstRegist = new HashMap<>();
			mapBodyFirstRegist.put("isFirstRegist", false);
			WsResponse responsesisFirstRegist = getResultWs(wmsParentalWrapper + "/registeration/update-data-user",
					mapBodyFirstRegist, HttpMethod.PUT, headerMapUpdateFirstRegist);

			mapBody.put("valid", true);
			mapBody.put("reason", "Registrasi ESBN berhasil");
			mapBody.put("bean", bean);

			session.removeAttribute("otp_failed_regesbn");
			session.removeAttribute("otp_timer_regesbn");
			session.removeAttribute("otp_start_regesbn");
			session.removeAttribute("otp_check_regesbn");

			return mapBody;
		} else {
			mapBody.put("reason", "Terjadi kesalahan di sistem kami. Segera hubungi Cabang Bank Mega terdekat.");
			mapBody.put("valid", false);
			return mapBody;
		}

	}

	@SuppressWarnings({ "unchecked" })
	@PostMapping("/addnew-rekeningdana")
	public Map<String, Object> addNewRekeningDana(@RequestBody RekeningDanaInvestor danaInvestor, Authentication auth,
			HttpSession session) {
		Map<String, Object> mapBody = new HashMap<String, Object>();

		int timerOtp = (int) session.getAttribute("otp_timer_addrekdana");
		int wrongOtpAnswer = (int) session.getAttribute("otp_falied_addrekdana");
		long timerOtpStart = (long) session.getAttribute("otp_start_addrekdana");
		Float porcessCallWs = (System.currentTimeMillis() - timerOtpStart) / 1000F;

		System.out.println("otp wrong : " + new Gson().toJson(wrongOtpAnswer));
		System.out.println("otp timer : " + new Gson().toJson(porcessCallWs));

		String otpCheck = (String) session.getAttribute("otp_check_addrekdana");
		System.out.println(otpCheck);

		if (!"addRekeningDana".equalsIgnoreCase(otpCheck)) {
			mapBody.put("valid", false);
			mapBody.put("reason", "Mohon minta kode OTP terlebih dahulu");
			return mapBody;
		}

		if (callOtp(session, "addRekeningDana", "") == null || callOtp(session, "addRekeningDana", "") == "") {
			mapBody.put("valid", false);
			mapBody.put("reason", "Mohon isi kode OTP terlebih dahulu");
			return mapBody;
		}

		if (timerOtp <= 0 || porcessCallWs >= 60) {
			mapBody.put("valid", false);
			mapBody.put("reason", "Kode OTP yang anda minta sudah kadaluarsa. Mohon minta kembali kode OTP anda");
			return mapBody;
		}

		if (wrongOtpAnswer <= 0) {
			mapBody.put("valid", false);
			mapBody.put("reason", "Anda sudah melakukan 3 kali kesalahan. Mohon minta kembali kode OTP anda");
			
			session.removeAttribute("otp_start_addrekdana");
			session.removeAttribute("otp_falied_addrekdana");
			session.removeAttribute("otp_start_addrekdana");
			session.removeAttribute("otp_check_addrekdana");
			session.removeAttribute("addRekeningDana");
			
			return mapBody;
		}

		if (danaInvestor.getMyOtp() == null) {
			mapBody.put("valid", false);
			mapBody.put("reason", "Mohon isi kode OTP terlebih dahulu");
			return mapBody;
		}

		if (!danaInvestor.getMyOtp().equalsIgnoreCase(callOtp(session, "addRekeningDana", ""))) {
			mapBody.put("valid", false);
			mapBody.put("reason", "Mohon maaf kode OTP yang Anda masukkan belum tepat.");
			session.setAttribute("otp_falied_addrekdana", wrongOtpAnswer - 1);
			return mapBody;
		}

		String bodyResult = "";
		if (danaInvestor.getNoRekTerpilihs() != null || danaInvestor.getNoRekTerpilihs().size() != 0) {
			ReqRekDana request = new ReqRekDana();
			for (RekeningDanaDummy iter : danaInvestor.getNoRekTerpilihs()) {
				request.setIdBank(iter.getNoRekTerpilih().getIdBank());
				request.setSid(danaInvestor.getSid());
				request.setNoRek(iter.getNoRekTerpilih().getNoRekening());
				request.setNama(danaInvestor.getNama());
			}
			HttpRestResponse result = wsBodyEsbnWithBody("/v1/rekeningdana", "POST", request, null,
					generateToken(auth));
			bodyResult = result.getBody();
		}

		/*
		 * headerMap.put("Authorization", tokenWrapper.getToken_type() + " " +
		 * tokenWrapper.getAccess_token()); headerMap.put("secret-url",
		 * "/v1/rekeningdana"); headerMap.put("secret-method", "POST"); HttpRestResponse
		 * result = wsBody(wmsParentalWrapper + "/esbn-connector/call-url/with-body",
		 * danaInvestor, HttpMethod.POST, headerMap);
		 */

		try {
			ErrorHttpHandler handler = mapperJsonToSingleDto(bodyResult, ErrorHttpHandler.class);
			if (handler.getCode() != 0) {
				Map<String, Object> errorMap = new HashMap<>();
				if (handler.getCode() == 102 || handler.getCode() == 203) {
					errorMap.put("valid", false);
					errorMap.put("reason",
							"Terjadi kesalahan di sistem kami. Segera hubungi Cabang Bank Mega terdekat.");
					return errorMap;
				} else {
					errorMap.put("valid", false);
					errorMap.put("reason", handler.getMessage());
					return errorMap;
				}
			}
		} catch (Exception e1) {
// TODO Auto-generated catch block
			e1.printStackTrace();
			mapBody.put("valid", false);
			mapBody.put("reason", bodyResult);
			return mapBody;
		}

		if (bodyResult != null) {

			mapBody = mapperJsonToHashMap(bodyResult);
			List<Map<String, String>> listRekeningDana = (List<Map<String, String>>) mapBody.get("RekeningDana");
			mapBody.put("valid", true);
			mapBody.put("listRekeningDana", listRekeningDana);

			session.removeAttribute("otp_falied_addrekdana");
			session.removeAttribute("otp_timer_addrekdana");
			session.removeAttribute("otp_start_addrekdana");
			session.removeAttribute("otp_check_addrekdana");

			return mapBody;

		} else {
			mapBody.put("reason", "Terjadi kesalahan di sistem kami. Segera hubungi Cabang Bank Mega terdekat.");
			mapBody.put("valid", false);
			return mapBody;
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Map<String, Object> checkRekDana(Authentication auth, RequestBodyRegistrationEsbn request) {
		Map<String, Object> mapBody = new HashMap<String, Object>();
		TokenWrapper tokenWrapper = getMyToken(auth);

		HttpRestResponse wsResponse = wsBodyWms("/Inquiry.svc/GetCustomer/" + auth.getName().toUpperCase(), "GET", null,
				null, tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());

		Map<String, Object> mapp = mapperJsonToHashMap(wsResponse.getBody());
		Map<String, Object> mappp = mapperJsonToHashMap(new Gson().toJson(mapp.get("Result")));

		String noRekeningDana = null;
		String idBank = null;
		List<Map<String, String>> listRekening = (List<Map<String, String>>) mappp.get("RekeningDana");
		List<RekeningDana> listRekDana = new ArrayList<RekeningDana>();

		for (Map mapRek : listRekening) {
			RekeningDana dana = new RekeningDana();

			noRekeningDana = (String) mapRek.get("NoRekening");
			idBank = (String) mapRek.get("IdBank");

			dana.setNoRekening(noRekeningDana);
			dana.setIdBank(idBank);
			dana.setIdBankNorek(noRekeningDana + "||" + idBank);

			if (!request.getNoRekTerpilih().getNoRekening().equalsIgnoreCase(dana.getNoRekening())) {
				mapBody.put("reason",
						"Rekening yang di daftarkan investor berbeda dengan di sistem kami. Segera hubungi Cabang Bank Mega terdekat.");
				mapBody.put("valid", false);
				return mapBody;
			}

			listRekDana.add(dana);

		}

		return mapBody;
	}

	/*
	 * @SuppressWarnings({ "unchecked" })
	 * 
	 * @PostMapping("/addnew-rekeningsb") public Map<String, Object>
	 * addNewRekeningSB(@RequestBody RekeningSuratBerhargaInvestor
	 * suratBerhargaInvestor, Authentication auth, HttpSession session) {
	 * 
	 * Map<String, Object> mapBody = new HashMap<String, Object>();
	 * 
	 * if (suratBerhargaInvestor.getMyOtp() == null) { mapBody.put("valid", false);
	 * mapBody.put("reason", "Mohon isi OTP terlebih dahulu"); return mapBody; }
	 * 
	 * if (!suratBerhargaInvestor.getMyOtp().equalsIgnoreCase(callOtp(session,
	 * "addRekeningSB", ""))) { mapBody.put("valid", false); mapBody.put("reason",
	 * "Mohon maaf OTP yang Anda masukkan belum tepat."); return mapBody; }
	 * 
	 * 
	 * headerMap.put("Authorization", tokenWrapper.getToken_type() + " " +
	 * tokenWrapper.getAccess_token()); headerMap.put("secret-url",
	 * "/v1/rekeningsb"); headerMap.put("secret-method", "POST"); HttpRestResponse
	 * result = wsBody(wmsParentalWrapper + "/esbn-connector/call-url/with-body",
	 * suratBerhargaInvestor, HttpMethod.POST, headerMap);
	 * 
	 * 
	 * String bodyResult = null;
	 * 
	 * if (suratBerhargaInvestor.getNoRekTerpilihSB() != null ||
	 * suratBerhargaInvestor.getNoRekTerpilihSB().size() != 0) { ReqRekSb rekSb =
	 * new ReqRekSb(); for (RekeningSBTerpilihDummy iter :
	 * suratBerhargaInvestor.getNoRekTerpilihSB()) {
	 * rekSb.setIdPartisipan(iter.getNoRekTerpilih().getIdPartisipan());
	 * rekSb.setIdSubregistry(iter.getNoRekTerpilih().getIdSubregistry());
	 * rekSb.setSid(suratBerhargaInvestor.getSid());
	 * rekSb.setNoRek(iter.getNoRekTerpilih().getNoRekening());
	 * rekSb.setNama(suratBerhargaInvestor.getNama()); } HttpRestResponse result =
	 * wsBodyEsbnWithBody("/v1/rekeningsb", "POST", rekSb, null,
	 * generateToken(auth)); bodyResult = result.getBody(); }
	 * 
	 * try { ErrorHttpHandler handler = mapperJsonToSingleDto(bodyResult,
	 * ErrorHttpHandler.class); if (handler.getCode() != 0) { Map<String, Object>
	 * errorMap = new HashMap<>(); if (handler.getCode() == 102 || handler.getCode()
	 * == 303) { errorMap.put("valid", false); errorMap.put("reason",
	 * "Terjadi kesalahan di sistem kami. Segera hubungi Cabang Bank Mega terdekat."
	 * ); return errorMap; } else { errorMap.put("valid", false);
	 * errorMap.put("reason",
	 * "Tidak dapat menambahkan rekening surat berharga karena : " +
	 * handler.getMessage()); return errorMap; }
	 * 
	 * } } catch (Exception e1) { // TODO Auto-generated catch block
	 * e1.printStackTrace(); mapBody.put("valid", false); mapBody.put("reason",
	 * bodyResult); return mapBody; }
	 * 
	 * if (bodyResult != null) {
	 * 
	 * mapBody = mapperJsonToHashMap(bodyResult); List<Map<String, String>>
	 * listRekeningSB = (List<Map<String, String>>) mapBody.get("RekeningSB");
	 * mapBody.put("valid", true); mapBody.put("listRekeningSB", listRekeningSB);
	 * return mapBody;
	 * 
	 * } else { mapBody.put("valid", false); mapBody.put("reason",
	 * "Terjadi kesalahan di sistem kami. Segera hubungi Cabang Bank Mega terdekat."
	 * ); return mapBody; }
	 * 
	 * }
	 */
}
