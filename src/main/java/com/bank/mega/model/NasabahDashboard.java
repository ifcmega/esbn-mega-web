package com.bank.mega.model;

import java.util.List;

public class NasabahDashboard {
	private String alamat;
	private String email;
	private String nama;
	private String noHandphone;
	private String noKtp;
	private String rekeningDana;
	private String rekeningSuratBerharga;
	private String riskProfile;
	private String sid;
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getNoHandphone() {
		return noHandphone;
	}
	public void setNoHandphone(String noHandphone) {
		this.noHandphone = noHandphone;
	}
	public String getNoKtp() {
		return noKtp;
	}
	public void setNoKtp(String noKtp) {
		this.noKtp = noKtp;
	}
	public String getRekeningDana() {
		return rekeningDana;
	}
	public void setRekeningDana(String rekeningDana) {
		this.rekeningDana = rekeningDana;
	}
	public String getRekeningSuratBerharga() {
		return rekeningSuratBerharga;
	}
	public void setRekeningSuratBerharga(String rekeningSuratBerharga) {
		this.rekeningSuratBerharga = rekeningSuratBerharga;
	}
	
	public String getRiskProfile() {
		return riskProfile;
	}
	public void setRiskProfile(String riskProfile) {
		this.riskProfile = riskProfile;
	}
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
}
