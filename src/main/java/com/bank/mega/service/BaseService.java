package com.bank.mega.service;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import com.bank.mega.UltimateBase;
import com.bank.mega.bean.EsbnRegistrationBean;
import com.bank.mega.bean.ProfileInvestor;
import com.bank.mega.bean.RekeningDana;
import com.bank.mega.bean.auth.TokenWrapper;
import com.bank.mega.model.TblUserEsbn;
import com.bank.mega.security.CustomUserService;
import com.bank.mega.security.HttpRestResponse;
import com.bank.mega.security.WsResponse;
import com.google.gson.Gson;

@Component
public class BaseService extends UltimateBase {

	@Autowired
	protected SessionRegistry sessionRegistry;

	protected final String USER_WILL_REGIST = "user-will-regist";

	protected final String OTP_CHECKER = "otp-checker";

	protected final String OTP_START_TIME = "otp-start-time";

	protected final String OTP_TIMER = "otp-timer";

	protected final String OTP_WRONG_ANSWER = "otp-wrong-answer";

	
	// otp starter --transaksi pemesanan
	protected final String OTP_START_TIME_PEMESANAN = "otp-start-time-pemesanan";

	protected final String OTP_TIMER_PEMESANAN = "otp-timer-pemesanan";

	protected final String OTP_WRONG_ANSWER_PEMESANAN = "otp-wrong-answer-pemesanan";
	// otp end --transaksi pemesanan

	
	// otp starter --transaksi early redemption
	protected final String OTP_START_TIME_EARLY = "otp-start-time_early";

	protected final String OTP_TIMER_EARLY = "otp-timer_early";

	protected final String OTP_WRONG_ANSWER_EARLY = "otp-wrong-answer_early";
	// otp end --transaksi early redemption

	protected TblUserEsbn getTblUser(String userSid, TokenWrapper tokenWrapper) {
		TblUserEsbn tblUserEsbn = new TblUserEsbn();

		Map<String, String> headerMap = new HashMap<>();
		headerMap.put("secret-user", userSid);
		headerMap.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());

		WsResponse response = getResultWs(wmsParentalWrapper + "/registeration/get-user", null, HttpMethod.GET,
				headerMap);
		try {
			tblUserEsbn = mapperJsonToSingleDto(response.getWsContent(), TblUserEsbn.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return tblUserEsbn;
	}

	protected TblUserEsbn getTblUser(Authentication auth) {
		TblUserEsbn tblUserEsbn = new TblUserEsbn();

		TokenWrapper tokenWrapper = getMyToken(auth);

		Map<String, String> headerMap = new HashMap<>();
		headerMap.put("secret-user", auth.getName());
		headerMap.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());

		WsResponse response = getResultWs(wmsParentalWrapper + "/registeration/get-user", null, HttpMethod.GET,
				headerMap);
		try {
			tblUserEsbn = mapperJsonToSingleDto(response.getWsContent(), TblUserEsbn.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return tblUserEsbn;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ProfileInvestor getInvestorBySidNew(Authentication auth) throws ParseException {
		Map<String, Object> map = new HashMap<String, Object>();
		ProfileInvestor investor = new ProfileInvestor();

		TokenWrapper tokenWrapper = getMyToken(auth);
		Map<String, String> headerMap = new HashMap<>();

		headerMap.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		// headerMap.put("secret-url", "/v1/investor/" + "sid48");
		headerMap.put("secret-url", "/v1/investor/" + auth.getName());
		headerMap.put("secret-method", "GET");
		HttpRestResponse result = wsBody(wmsParentalWrapper + "esbn-connector/call-url/no-body", null, HttpMethod.GET,
				headerMap);

		String bodyResult = result.getBody();

		if (bodyResult != null) {

			Map<String, Object> mapBody = mapperJsonToHashMap(bodyResult);

			if (mapBody.get("Sid") == null) {
				return null;
			}

			investor.setAlamat((String) mapBody.get("Alamat"));
			investor.setSid((String) mapBody.get("Sid"));
			investor.setNama((String) mapBody.get("Nama"));
			investor.setNoIdentitas((String) mapBody.get("NoIdentitas"));
			investor.setTempatLahir((String) mapBody.get("TempatLahir"));

			// formated date tgl lahir
			String tglLahir = (String) mapBody.get("TglLahir");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date d = sdf.parse(tglLahir);
			String formattedDate = output.format(d);

			investor.setTglLahir(formattedDate);
			investor.setJenisKelamin((String) mapBody.get("JenisKelamin"));
			investor.setPekerjaan((String) mapBody.get("Pekerjaan"));
			investor.setProvinsi((String) mapBody.get("Provinsi"));
			investor.setKota((String) mapBody.get("Kota"));

			investor.setNoHp((String) mapBody.get("NoHp"));
			investor.setEmail((String) mapBody.get("Email"));

			String noRekeningDana = null;
			String noRekeningSurat = null;
			String namaSubregistry = null;
			String namaBank = null;
			String notlp = null;

			notlp = (String) mapBody.get("NoTelp");

			if (notlp != null) {
				investor.setNoTelp(notlp);
			} else {
				notlp = "";
				investor.setNoTelp(notlp);
			}

			List<Map<String, String>> listRekening = (List<Map<String, String>>) mapBody.get("RekeningDana");

			List<RekeningDana> listRekDana = new ArrayList<RekeningDana>();
			for (Map mapRek : listRekening) {
				RekeningDana dana = new RekeningDana();
				noRekeningDana = (String) mapRek.get("NoRek");
				namaBank = (String) mapRek.get("NamaBank");
				dana.setNoRekening(noRekeningDana);
				dana.setBank(namaBank);
				listRekDana.add(dana);
			}
			List<Map<String, String>> listRekeningSurat = (List<Map<String, String>>) mapBody.get("RekeningSB");

			if (listRekeningSurat.isEmpty() || "[]".equals(listRekeningSurat) || null == listRekeningSurat) {
				noRekeningSurat = "";
				namaSubregistry = "";

			} else {
				noRekeningSurat = listRekeningSurat.get(0).get("NoRek");
				namaSubregistry = listRekeningSurat.get(0).get("NamaSubregistry");
			}
			investor.setNamaSubregistry(namaSubregistry);
			investor.setRekeningSB(noRekeningSurat);
			investor.setRekeningDana(listRekDana);
			map.put("investor", investor);
			map.put("valid", true);

		} else {
			map.put("valid", false);
			map.put("reason", "Terjadi kesalahan di sistem kami. Segera hubungi Cabang Bank Mega terdekat.");
		}

		return investor;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ProfileInvestor getInvestorBySidNewInLogin(String userSid) throws ParseException {
		Map<String, Object> map = new HashMap<String, Object>();
		ProfileInvestor investor = new ProfileInvestor();

		TokenWrapper tokenWrapper = getMyToken(ADDITION_USER, ADDITION_PASS, "visitor");
		Map<String, String> headerMap = new HashMap<>();

		headerMap.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		// headerMap.put("secret-url", "/v1/investor/" + "sid48");
		headerMap.put("secret-url", "/v1/investor/" + userSid);
		headerMap.put("secret-method", "GET");
		HttpRestResponse result = wsBody(wmsParentalWrapper + "esbn-connector/call-url/no-body", null, HttpMethod.GET,
				headerMap);

		String bodyResult = result.getBody();
//System.err.println("body result investor " + bodyResult);
		if (bodyResult != null) {
			Map<String, Object> mapBody = mapperJsonToHashMap(bodyResult);
			if (mapBody.get("Code") != null && (int) mapBody.get("Code") == 102) {
				return null;
			}
			investor.setAlamat((String) mapBody.get("Alamat"));
			investor.setSid((String) mapBody.get("Sid"));
			investor.setNama((String) mapBody.get("Nama"));
			investor.setNoIdentitas((String) mapBody.get("NoIdentitas"));
			investor.setTempatLahir((String) mapBody.get("TempatLahir"));

			// formated date tgl lahir
			String tglLahir = (String) mapBody.get("TglLahir");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date d = sdf.parse(tglLahir);
			String formattedDate = output.format(d);

			investor.setTglLahir(formattedDate);
			investor.setJenisKelamin((String) mapBody.get("JenisKelamin"));
			investor.setPekerjaan((String) mapBody.get("Pekerjaan"));
			investor.setProvinsi((String) mapBody.get("Provinsi"));
			investor.setKota((String) mapBody.get("Kota"));

			investor.setNoHp((String) mapBody.get("NoHp"));
			investor.setEmail((String) mapBody.get("Email"));

			String noRekeningDana = null;
			String noRekeningSurat = null;
			String namaSubregistry = null;
			String namaBank = null;
			String notlp = null;

			notlp = (String) mapBody.get("NoTelp");

			if (notlp != null) {
				investor.setNoTelp(notlp);
			} else {
				notlp = "";
				investor.setNoTelp(notlp);
			}

			List<Map<String, String>> listRekening = (List<Map<String, String>>) mapBody.get("RekeningDana");

			List<RekeningDana> listRekDana = new ArrayList<RekeningDana>();
			for (Map mapRek : listRekening) {
				RekeningDana dana = new RekeningDana();
				noRekeningDana = (String) mapRek.get("NoRek");
				namaBank = (String) mapRek.get("NamaBank");
				dana.setNoRekening(noRekeningDana);
				dana.setBank(namaBank);
				listRekDana.add(dana);
			}
			List<Map<String, String>> listRekeningSurat = (List<Map<String, String>>) mapBody.get("RekeningSB");

			if (listRekeningSurat.isEmpty() || "[]".equals(listRekeningSurat) || null == listRekeningSurat) {
				noRekeningSurat = "";
				namaSubregistry = "";

			} else {
				noRekeningSurat = listRekeningSurat.get(0).get("NoRek");
				namaSubregistry = listRekeningSurat.get(0).get("NamaSubregistry");
			}
			investor.setNamaSubregistry(namaSubregistry);
			investor.setRekeningSB(noRekeningSurat);
			investor.setRekeningDana(listRekDana);
			map.put("investor", investor);
			map.put("valid", true);

		} else {
			map.put("valid", false);
			map.put("reason", "Terjadi kesalahan di sistem kami. Segera hubungi Cabang Bank Mega terdekat.");
		}

		return investor;
	}

	protected void checkSessionValidity(Authentication authentication, HttpServletRequest request,
			HttpServletResponse response) {

		System.err.println("masuk ke checkSessionValidity from webservice");
		for (Object principal : sessionRegistry.getAllPrincipals()) {
			if (principal instanceof CustomUserService) {
				CustomUserService service = (CustomUserService) principal;
				System.err.println("cekking : " + new Gson().toJson(service) + "     " + authentication.getName());
				if (service.getUsername().equalsIgnoreCase(authentication.getName())) {
					return;
				}
			}
		}

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		try {
			System.err.println("masuk ke redirect");
			response.sendRedirect("/login");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return;
	}

	protected String checkSessionValidity(String url, Authentication authentication, HttpServletRequest request,
			HttpServletResponse response) {

		System.err.println("masuk ke checkSessionValidity");
		for (Object principal : sessionRegistry.getAllPrincipals()) {
			if (principal instanceof CustomUserService) {
				CustomUserService service = (CustomUserService) principal;
				System.err.println("cekking : " + new Gson().toJson(service) + "     " + authentication.getName());
				if (service.getUsername().equalsIgnoreCase(authentication.getName())) {
					return url;
				}
			}
		}

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:8080/login";
	}

	protected void informationHeader(Model model, Authentication authentication) {
		String namaAuthoritie = new Gson().toJson(authentication.getAuthorities()).replace("[", "").replace("]", "");
		Map<String, Object> mapp = mapperJsonToHashMap(namaAuthoritie);
		model.addAttribute("nama_nasabah", mapp.get("role"));
		System.err.println("liat user dong : " + new Gson().toJson(getTblUser(authentication)));
		boolean open = true;
		if (getTblUser(authentication).getIsFirstEsbnRegist()) {
			open = false;
		}
		System.err.println("open kah : " + open);
		model.addAttribute("aktive_menu", open);
		model.addAttribute("kelengkapan_nasabah", mapp.get("role") + " - " + authentication.getName());
	}

	protected boolean isMenuAccessable(Authentication authentication) {
		boolean open = true;
		if (getTblUser(authentication).getIsFirstEsbnRegist()) {
			open = false;
		}
		System.err.println("open kah : " + open);
		return open;
	}

	protected String callOtp(HttpSession session, String menu, String partisi) {

		return (String) session.getAttribute(menu + partisi);
	}

	protected Map<String, Object> WmsInfo(Authentication authentication) {

		TokenWrapper tokenWrapper = getMyToken(authentication);
		Map<String, String> headerMap = new HashMap<>();
		headerMap.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		HttpRestResponse responseWms = wsBodyWms("/Inquiry.svc/GetCustomer/" + authentication.getName(), "GET", null,
				null);
		Map<String, Object> mapWms = mapperJsonToHashMap(responseWms.getBody());
		Map<String, Object> mapResult = mapperJsonToHashMap(new Gson().toJson(mapWms.get("Result")));

		return mapResult;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public EsbnRegistrationBean getWmsCustomerAndInvestorValueBySid(Authentication auth) throws ParseException {
		EsbnRegistrationBean bean = new EsbnRegistrationBean();
		TokenWrapper tokenWrapper = getMyToken(auth);
		Map<String, Object> mapingktp = new HashMap<>();
		Map<String, String> headerKTP = new HashMap<>();
		Map<String, Object> mapp = new HashMap<>();
		Map<String, Object> mappp = new HashMap<>();
		Map<String, Object> mapper = new HashMap<>();
		TblUserEsbn tblUserEsbn = getTblUser(auth);
		// tblUserEsbn.setIsFirstEsbnRegist(false);
		StringBuilder buffer = new StringBuilder();
		String koma = "";

		HttpRestResponse wsResponse = new HttpRestResponse();
		wsResponse = wsBodyWms("/Inquiry.svc/GetCustomer/" + auth.getName(), "GET", null, null,
				tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		mapp = mapperJsonToHashMap(wsResponse.getBody());
		mappp = mapperJsonToHashMap(new Gson().toJson(mapp.get("Result")));

		if (tblUserEsbn.getIsFirstEsbnRegist() == true) {
			// formated date tgl lahir
			String tglLahir = (String) mappp.get("TanggalLahir");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat output = new SimpleDateFormat("dd/MM/yyyy");
			java.util.Date d = sdf.parse(tglLahir);
			String formattedDate = output.format(d);

			bean.setAlamat((String) mappp.get("Alamat"));
			bean.setSid((String) mappp.get("SID"));
			// bean.setSid("sid44");
			bean.setNamaNasabah((String) mappp.get("Nama"));

			bean.setTempatLahir((String) mappp.get("TempatLahir"));
			bean.setTanggalLahir(formattedDate);
			bean.setJenisKelamin((String) mappp.get("JenisKelamin"));
			bean.setJenisPekerjaan((String) mappp.get("Pekerjaan"));
			bean.setProvinsi((String) mappp.get("Propinsi"));
			bean.setKota((String) mappp.get("Kota"));
			bean.setEmail((String) mappp.get("Email"));
			bean.setKodeJenisKelamin((String) mappp.get("KodeJenisKelamin"));
			bean.setKodeKota((String) mappp.get("KodeKota"));
			bean.setKodePekerjaan((String) mappp.get("KodePekerjaan"));

			String nohp = null;
			String notlp = null;
			notlp = (String) mappp.get("NoTelepon");
			nohp = (String) mappp.get("NoHandphone");
			if (nohp != null) {
				bean.setNoHandphone(nohp);
			} else {
				nohp = "";
				bean.setNoHandphone(nohp);
			}

			if (notlp != null) {
				bean.setNoTelepon(notlp);
			} else {
				notlp = "";
				bean.setNoTelepon(notlp);
			}

			String noRekeningDana = null;
			String idBank = null;
			List<Map<String, String>> listRekening = (List<Map<String, String>>) mappp.get("RekeningDana");
			List<RekeningDana> listRekDana = new ArrayList<RekeningDana>();
			try {
				for (Map mapRek : listRekening) {
					RekeningDana dana = new RekeningDana();
					noRekeningDana = (String) mapRek.get("NoRekening");
					idBank = (String) mapRek.get("IdBank");
					dana.setNoRekening(noRekeningDana);
					dana.setIdBank(idBank);
					listRekDana.add(dana);
				}

				for (RekeningDana data : listRekDana) {
					buffer.append(koma).append(data.getNoRekening());
					koma = "\n";
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			String noRekeningSurat = null;
			String namaSubregistry = null;
			String idPartisipan = null;
			String idSubregistry = null;
			String namaPartisipan = null;
			List<Map<String, String>> listRekeningSurat = (List<Map<String, String>>) mappp
					.get("RekeningSuratBerharga");

			if (listRekeningSurat.isEmpty() || "[]".equals(listRekeningSurat) || null == listRekeningSurat) {
				noRekeningSurat = "";
				namaSubregistry = "";
				idPartisipan = "";
				idSubregistry = "";
				namaPartisipan = "";

			} else {
				noRekeningSurat = listRekeningSurat.get(0).get("NoRekening");
				namaSubregistry = listRekeningSurat.get(0).get("NamaSubRegistry");
				idPartisipan = listRekeningSurat.get(0).get("IdPartisipan");
				idSubregistry = listRekeningSurat.get(0).get("IdSubregistry");
				namaPartisipan = listRekeningSurat.get(0).get("NamaPartisipan");
			}
			bean.setIdPartisipan(idPartisipan);
			bean.setIdSubregistry(idSubregistry);
			bean.setNamaPartisipan(namaPartisipan);
			bean.setNamaSubregistry(namaSubregistry);
			bean.setNoRekSuratBerharga(noRekeningSurat);
			bean.setNoRekeningDana(listRekDana);
			bean.setRekDanaString(buffer.toString());

		} else {
			Map<String, String> headerMap = new HashMap<>();

			headerMap.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
			// headerMap.put("secret-url", "/v1/investor/" + "sid48");
			headerMap.put("secret-url", "/v1/investor/" + auth.getName().toUpperCase());
			headerMap.put("secret-method", "GET");
			wsResponse = wsBody(wmsParentalWrapper + "esbn-connector/call-url/no-body", null, HttpMethod.GET,
					headerMap);

			mapper = mapperJsonToHashMap(wsResponse.getBody());

			// formated date tgl lahir
			String tglLahir = (String) mapper.get("TglLahir");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			SimpleDateFormat output = new SimpleDateFormat("dd/MM/yyyy");
			java.util.Date d = sdf.parse(tglLahir);
			String formattedDate = output.format(d);

			bean.setAlamat((String) mapper.get("Alamat"));
			bean.setSid((String) mapper.get("Sid"));
			// bean.setSid("sid44");
			bean.setNamaNasabah((String) mapper.get("Nama"));

			bean.setTempatLahir((String) mapper.get("TempatLahir"));
			bean.setTanggalLahir(formattedDate);
			bean.setJenisKelamin((String) mapper.get("JenisKelamin"));
			bean.setJenisPekerjaan((String) mapper.get("Pekerjaan"));
			bean.setProvinsi((String) mapper.get("Provinsi"));
			bean.setKota((String) mapper.get("Kota"));
			bean.setEmail(tblUserEsbn.getUserEmail());
			bean.setKodeJenisKelamin((String) mapper.get("KdJenisKelamin"));
			bean.setKodeKota((String) mapper.get("KdKota"));
			bean.setKodePekerjaan((String) mapper.get("KdPekerjaan"));

			String nohp = null;
			String notlp = null;
			notlp = (String) mapper.get("NoTelp");
			nohp = tblUserEsbn.getUserPhone();
			if (nohp != null) {
				bean.setNoHandphone(nohp);
			} else {
				nohp = "";
				bean.setNoHandphone(nohp);
			}

			if (notlp != null) {
				bean.setNoTelepon(notlp);
			} else {
				notlp = "";
				bean.setNoTelepon(notlp);
			}

			String noRekeningDana = null;
			Integer idBank = null;
			List<Map<String, String>> listRekening = (List<Map<String, String>>) mapper.get("RekeningDana");
			List<RekeningDana> listRekDana = new ArrayList<RekeningDana>();
			try {
				for (Map mapRek : listRekening) {
					RekeningDana dana = new RekeningDana();
					noRekeningDana = (String) mapRek.get("NoRek");
					idBank = (Integer) mapRek.get("IdBank");
					dana.setNoRekening(noRekeningDana);
					dana.setIdBank(idBank.toString());
					listRekDana.add(dana);
				}

				for (RekeningDana data : listRekDana) {
					buffer.append(koma).append(data.getNoRekening());
					koma = "\n";
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			String noRekeningSurat = null;
			String namaSubregistry = null;
			String idPartisipan = null;
			String idSubregistry = null;
			String namaPartisipan = null;
			List<Map<String, String>> listRekeningSurat = (List<Map<String, String>>) mapper.get("RekeningSB");

			if (listRekeningSurat.isEmpty() || "[]".equals(listRekeningSurat) || null == listRekeningSurat) {
				noRekeningSurat = "";
				namaSubregistry = "";
				idPartisipan = "";
				idSubregistry = "";
				namaPartisipan = "";

			} else {
				noRekeningSurat = listRekeningSurat.get(0).get("NoRek");
				namaSubregistry = listRekeningSurat.get(0).get("NamaSubregistry");
				idPartisipan = String.valueOf(listRekeningSurat.get(0).get("IdPartisipan"));
				idSubregistry = String.valueOf(listRekeningSurat.get(0).get("IdSubregistry"));
				namaPartisipan = listRekeningSurat.get(0).get("NamaPartisipan");
			}
			bean.setIdPartisipan(idPartisipan.toString());
			bean.setIdSubregistry(idSubregistry.toString());
			bean.setNamaPartisipan(namaPartisipan);
			bean.setNamaSubregistry(namaSubregistry);
			bean.setNoRekSuratBerharga(noRekeningSurat);
			bean.setNoRekeningDana(listRekDana);
			bean.setRekDanaString(buffer.toString());

		}
		headerKTP.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		headerKTP.put("secret-field", "EKTP");
		headerKTP.put("secret-sid", auth.getName());
		HttpRestResponse resultKtp = wsBody(wmsParentalWrapper + "/registeration/GetOneDetailUser", null,
				HttpMethod.GET, headerKTP);

		mapingktp = mapperJsonToHashMap(resultKtp.getBody());
		bean.setNoKtp((String) mapingktp.get("itemDescription"));
		return bean;
	}
}
