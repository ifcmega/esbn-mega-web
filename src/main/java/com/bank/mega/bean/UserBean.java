package com.bank.mega.bean;

import com.google.gson.Gson;

public class UserBean {
          private String sid;
          private String dateBirth;
          private String ektp;
          private String newPass;
          private String confirmPass;
          private String otpVerification;
          private String otpChecker;
          private String email;
          private String userName;
          private String userPhone;
      	private Boolean isActive;
    	private Boolean isFirstEsbnRegist;
    	
    	
          
		public String getUserPhone() {
			return userPhone;
		}
		public void setUserPhone(String userPhone) {
			this.userPhone = userPhone;
		}
		public Boolean getIsActive() {
			return isActive;
		}
		public void setIsActive(Boolean isActive) {
			this.isActive = isActive;
		}
		public Boolean getIsFirstEsbnRegist() {
			return isFirstEsbnRegist;
		}
		public void setIsFirstEsbnRegist(Boolean isFirstEsbnRegist) {
			this.isFirstEsbnRegist = isFirstEsbnRegist;
		}
		public String getUserName() {
			return userName;
		}
		public void setUserName(String userName) {
			this.userName = userName;
		}
		public String getOtpChecker() {
			return otpChecker;
		}
		public void setOtpChecker(String otpChecker) {
			this.otpChecker = otpChecker;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getNewPass() {
			return newPass;
		}
		public void setNewPass(String newPass) {
			this.newPass = newPass;
		}
		public String getConfirmPass() {
			return confirmPass;
		}
		public void setConfirmPass(String confirmPass) {
			this.confirmPass = confirmPass;
		}
		public String getOtpVerification() {
			return otpVerification;
		}
		public void setOtpVerification(String otpVerification) {
			this.otpVerification = otpVerification;
		}
		public String getDateBirth() {
			return dateBirth;
		}
		public void setDateBirth(String dateBirth) {
			this.dateBirth = dateBirth;
		}
		public String getEktp() {
			return ektp;
		}
		public void setEktp(String ektp) {
			this.ektp = ektp;
		}
		public String getSid() {
			return sid;
		}
		public void setSid(String sid) {
			this.sid = sid;
		}

		
		
//		public String toString(UserBean user) {
//			// TODO Auto-generated method stub
//			return new Gson().toJson(user);
//		}
          
         
}
