package com.bank.mega.bean;

import java.util.ArrayList;
import java.util.List;

public class EsbnInfoUser {
	private String JenisKelamin;
	private String Pekerjaan;
	private String Kota;
	private String Provinsi;
	private String Status;
	private String CreatedAt;
	private String CreatedBy;
	private String ModifiedAt;
	private String ModifiedBy;
	private List<EsbnRekeningDana> RekeningDana = new ArrayList<>();
	private List<EsbnRekeningSuratBerharga> RekeningSB = new ArrayList<>();
	private String Sid;
	private String Nama;
	private String NoIdentitas;
	private String TempatLahir;
	private String TglLahir;
	private String KdJenisKelamin;
	private String KdPekerjaan;
	private String KdKota;
	private String Alamat;
	private String NoTelp;
	private String NoHp;
	private String Email;

	public String getJenisKelamin() {
		return JenisKelamin;
	}

	public void setJenisKelamin(String jenisKelamin) {
		JenisKelamin = jenisKelamin;
	}

	public String getPekerjaan() {
		return Pekerjaan;
	}

	public void setPekerjaan(String pekerjaan) {
		Pekerjaan = pekerjaan;
	}

	public String getKota() {
		return Kota;
	}

	public void setKota(String kota) {
		Kota = kota;
	}

	public String getProvinsi() {
		return Provinsi;
	}

	public void setProvinsi(String provinsi) {
		Provinsi = provinsi;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getCreatedAt() {
		return CreatedAt;
	}

	public void setCreatedAt(String createdAt) {
		CreatedAt = createdAt;
	}

	public String getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}

	public String getModifiedAt() {
		return ModifiedAt;
	}

	public void setModifiedAt(String modifiedAt) {
		ModifiedAt = modifiedAt;
	}

	public String getModifiedBy() {
		return ModifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		ModifiedBy = modifiedBy;
	}

	

	public List<EsbnRekeningDana> getRekeningDana() {
		return RekeningDana;
	}

	public void setRekeningDana(List<EsbnRekeningDana> rekeningDana) {
		RekeningDana = rekeningDana;
	}

	public List<EsbnRekeningSuratBerharga> getRekeningSB() {
		return RekeningSB;
	}

	public void setRekeningSB(List<EsbnRekeningSuratBerharga> rekeningSB) {
		RekeningSB = rekeningSB;
	}

	public String getSid() {
		return Sid;
	}

	public void setSid(String sid) {
		Sid = sid;
	}

	public String getNama() {
		return Nama;
	}

	public void setNama(String nama) {
		Nama = nama;
	}

	public String getNoIdentitas() {
		return NoIdentitas;
	}

	public void setNoIdentitas(String noIdentitas) {
		NoIdentitas = noIdentitas;
	}

	public String getTempatLahir() {
		return TempatLahir;
	}

	public void setTempatLahir(String tempatLahir) {
		TempatLahir = tempatLahir;
	}

	public String getTglLahir() {
		return TglLahir;
	}

	public void setTglLahir(String tglLahir) {
		TglLahir = tglLahir;
	}

	public String getKdJenisKelamin() {
		return KdJenisKelamin;
	}

	public void setKdJenisKelamin(String kdJenisKelamin) {
		KdJenisKelamin = kdJenisKelamin;
	}

	public String getKdPekerjaan() {
		return KdPekerjaan;
	}

	public void setKdPekerjaan(String kdPekerjaan) {
		KdPekerjaan = kdPekerjaan;
	}

	public String getKdKota() {
		return KdKota;
	}

	public void setKdKota(String kdKota) {
		KdKota = kdKota;
	}

	public String getAlamat() {
		return Alamat;
	}

	public void setAlamat(String alamat) {
		Alamat = alamat;
	}

	public String getNoTelp() {
		return NoTelp;
	}

	public void setNoTelp(String noTelp) {
		NoTelp = noTelp;
	}

	public String getNoHp() {
		return NoHp;
	}

	public void setNoHp(String noHp) {
		NoHp = noHp;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

}
