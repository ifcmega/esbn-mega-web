package com.bank.mega.bean;

import java.util.List;

public class ProfileInvestor {
	private String JenisKelamin;
	private String Pekerjaan;
	private String Kota;
	private String Provinsi;
	private String Status;
	private String CreatedAt;
	private String CreatedBy;
	private String ModifiedAt;
	private String ModifiedBy;
	private List<RekeningDana> RekeningDana;
	private String RekeningDanaString;
	private String RekeningSB;
	private String Sid;
	private String Nama;
	private String NoIdentitas;
	private String TempatLahir;
	private String TglLahir;
	private String KdJenisKelamin;
	private String KdPekerjaan;
	private String KdKota;
	private String Alamat;
	private String NoTelp;
	private String NoHp;
	private String Email;
	private String NamaSubregistry;
	private String reason;
	private boolean valid;

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getJenisKelamin() {
		return JenisKelamin;
	}

	public void setJenisKelamin(String jenisKelamin) {
		JenisKelamin = jenisKelamin;
	}

	public String getPekerjaan() {
		return Pekerjaan;
	}

	public void setPekerjaan(String pekerjaan) {
		Pekerjaan = pekerjaan;
	}

	public String getKota() {
		return Kota;
	}

	public void setKota(String kota) {
		Kota = kota;
	}

	public String getProvinsi() {
		return Provinsi;
	}

	public void setProvinsi(String provinsi) {
		Provinsi = provinsi;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getCreatedAt() {
		return CreatedAt;
	}

	public void setCreatedAt(String createdAt) {
		CreatedAt = createdAt;
	}

	public String getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}

	public String getModifiedAt() {
		return ModifiedAt;
	}

	public void setModifiedAt(String modifiedAt) {
		ModifiedAt = modifiedAt;
	}

	public String getRekeningDanaString() {
		return RekeningDanaString;
	}

	public void setRekeningDanaString(String rekeningDanaString) {
		RekeningDanaString = rekeningDanaString;
	}

	public String getModifiedBy() {
		return ModifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		ModifiedBy = modifiedBy;
	}

	public List<RekeningDana> getRekeningDana() {
		return RekeningDana;
	}

	public void setRekeningDana(List<RekeningDana> rekeningDana) {
		RekeningDana = rekeningDana;
	}

	public String getRekeningSB() {
		return RekeningSB;
	}

	public void setRekeningSB(String rekeningSB) {
		RekeningSB = rekeningSB;
	}

	public String getSid() {
		return Sid;
	}

	public void setSid(String sid) {
		Sid = sid;
	}

	public String getNama() {
		return Nama;
	}

	public void setNama(String nama) {
		Nama = nama;
	}

	public String getNoIdentitas() {
		return NoIdentitas;
	}

	public void setNoIdentitas(String noIdentitas) {
		NoIdentitas = noIdentitas;
	}

	public String getTempatLahir() {
		return TempatLahir;
	}

	public void setTempatLahir(String tempatLahir) {
		TempatLahir = tempatLahir;
	}

	public String getTglLahir() {
		return TglLahir;
	}

	public void setTglLahir(String tglLahir) {
		TglLahir = tglLahir;
	}

	public String getKdJenisKelamin() {
		return KdJenisKelamin;
	}

	public void setKdJenisKelamin(String kdJenisKelamin) {
		KdJenisKelamin = kdJenisKelamin;
	}

	public String getKdPekerjaan() {
		return KdPekerjaan;
	}

	public void setKdPekerjaan(String kdPekerjaan) {
		KdPekerjaan = kdPekerjaan;
	}

	public String getKdKota() {
		return KdKota;
	}

	public void setKdKota(String kdKota) {
		KdKota = kdKota;
	}

	public String getAlamat() {
		return Alamat;
	}

	public void setAlamat(String alamat) {
		Alamat = alamat;
	}

	public String getNoTelp() {
		return NoTelp;
	}

	public void setNoTelp(String noTelp) {
		NoTelp = noTelp;
	}

	public String getNoHp() {
		return NoHp;
	}

	public void setNoHp(String noHp) {
		NoHp = noHp;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getNamaSubregistry() {
		return NamaSubregistry;
	}

	public void setNamaSubregistry(String namaSubregistry) {
		NamaSubregistry = namaSubregistry;
	}
}
