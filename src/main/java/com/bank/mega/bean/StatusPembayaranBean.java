package com.bank.mega.bean;

public class StatusPembayaranBean {
    private String KodePemesanan;
    private String KodeBilling;
    private String NTPN;
    private String NTB;
    private String TglJamPembayaran;
    private String BankPresepsi;
    private String ChannelPembayaran;
    private String trxId;
    
    
	public String getTrxId() {
		return trxId;
	}
	public void setTrxId(String trxId) {
		this.trxId = trxId;
	}
	public String getKodePemesanan() {
		return KodePemesanan;
	}
	public void setKodePemesanan(String kodePemesanan) {
		KodePemesanan = kodePemesanan;
	}
	public String getKodeBilling() {
		return KodeBilling;
	}
	public void setKodeBilling(String kodeBilling) {
		KodeBilling = kodeBilling;
	}
	public String getNTPN() {
		return NTPN;
	}
	public void setNTPN(String nTPN) {
		NTPN = nTPN;
	}
	public String getNTB() {
		return NTB;
	}
	public void setNTB(String nTB) {
		NTB = nTB;
	}
	public String getTglJamPembayaran() {
		return TglJamPembayaran;
	}
	public void setTglJamPembayaran(String tglJamPembayaran) {
		TglJamPembayaran = tglJamPembayaran;
	}
	public String getBankPresepsi() {
		return BankPresepsi;
	}
	public void setBankPresepsi(String bankPresepsi) {
		BankPresepsi = bankPresepsi;
	}
	public String getChannelPembayaran() {
		return ChannelPembayaran;
	}
	public void setChannelPembayaran(String channelPembayaran) {
		ChannelPembayaran = channelPembayaran;
	}
    
    
}
