package com.bank.mega.bean;

import java.util.Date;

public class TblEsbnPemesananTransaksiBean {
	private Long trxId;

	private String transaksiPemesanan;

	private String transaksiStatus;

	private String transaksiCode;

	private Date createdDate;

	private String createdBy;

	private Date updatedDate;

	private String updatedBy;

	public Long getTrxId() {
		return trxId;
	}

	public void setTrxId(Long trxId) {
		this.trxId = trxId;
	}

	public String getTransaksiPemesanan() {
		return transaksiPemesanan;
	}

	public void setTransaksiPemesanan(String transaksiPemesanan) {
		this.transaksiPemesanan = transaksiPemesanan;
	}

	

	public String getTransaksiStatus() {
		return transaksiStatus;
	}

	public void setTransaksiStatus(String transaksiStatus) {
		this.transaksiStatus = transaksiStatus;
	}

	public String getTransaksiCode() {
		return transaksiCode;
	}

	public void setTransaksiCode(String transaksiCode) {
		this.transaksiCode = transaksiCode;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
}
