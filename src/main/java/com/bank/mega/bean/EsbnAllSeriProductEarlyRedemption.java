package com.bank.mega.bean;

import java.math.BigDecimal;
import java.math.BigInteger;

public class EsbnAllSeriProductEarlyRedemption {
     
	private Long Id;
	private String Seri;
	private String TglSetelmen;
	private String TglMulaiRedeem;
	private String TglAkhirRedeem;
	private BigDecimal MinRedeem;
	private BigDecimal KelipatanRedeem;
	private BigInteger MaxPcent;
	private BigDecimal MinKepemilikan;
	private Long FrekuensiKupon;
	private String periodeRedemption;
	
	
	
	public String getPeriodeRedemption() {
		return periodeRedemption;
	}
	public void setPeriodeRedemption(String periodeRedemption) {
		this.periodeRedemption = periodeRedemption;
	}
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getSeri() {
		return Seri;
	}
	public void setSeri(String seri) {
		Seri = seri;
	}
	public String getTglSetelmen() {
		return TglSetelmen;
	}
	public void setTglSetelmen(String tglSetelmen) {
		TglSetelmen = tglSetelmen;
	}
	public String getTglMulaiRedeem() {
		return TglMulaiRedeem;
	}
	public void setTglMulaiRedeem(String tglMulaiRedeem) {
		TglMulaiRedeem = tglMulaiRedeem;
	}
	public String getTglAkhirRedeem() {
		return TglAkhirRedeem;
	}
	public void setTglAkhirRedeem(String tglAkhirRedeem) {
		TglAkhirRedeem = tglAkhirRedeem;
	}
	public BigDecimal getMinRedeem() {
		return MinRedeem;
	}
	public void setMinRedeem(BigDecimal minRedeem) {
		MinRedeem = minRedeem;
	}
	public BigDecimal getKelipatanRedeem() {
		return KelipatanRedeem;
	}
	public void setKelipatanRedeem(BigDecimal kelipatanRedeem) {
		KelipatanRedeem = kelipatanRedeem;
	}
	
	public BigInteger getMaxPcent() {
		return MaxPcent;
	}
	public void setMaxPcent(BigInteger maxPcent) {
		MaxPcent = maxPcent;
	}
	public BigDecimal getMinKepemilikan() {
		return MinKepemilikan;
	}
	public void setMinKepemilikan(BigDecimal minKepemilikan) {
		MinKepemilikan = minKepemilikan;
	}
	public Long getFrekuensiKupon() {
		return FrekuensiKupon;
	}
	public void setFrekuensiKupon(Long frekuensiKupon) {
		FrekuensiKupon = frekuensiKupon;
	}
	
	
	
}
