package com.bank.mega.bean;

import java.util.List;

public class RekeningDanaInvestor {
	private String IdBank;
	private String Sid;
	private String NoRek;
	private String Nama;
	private String myOtp;
	private RekeningDana noRekTerpilih;
	private List<RekeningDanaDummy> noRekTerpilihs;

	
	
	
	

	public List<RekeningDanaDummy> getNoRekTerpilihs() {
		return noRekTerpilihs;
	}

	public void setNoRekTerpilihs(List<RekeningDanaDummy> noRekTerpilihs) {
		this.noRekTerpilihs = noRekTerpilihs;
	}

	public RekeningDana getNoRekTerpilih() {
		return noRekTerpilih;
	}

	public void setNoRekTerpilih(RekeningDana noRekTerpilih) {
		this.noRekTerpilih = noRekTerpilih;
	}

	public String getMyOtp() {
		return myOtp;
	}

	public void setMyOtp(String myOtp) {
		this.myOtp = myOtp;
	}
	public String getIdBank() {
		return IdBank;
	}

	public void setIdBank(String idBank) {
		IdBank = idBank;
	}

	public String getSid() {
		return Sid;
	}

	public void setSid(String sid) {
		Sid = sid;
	}

	public String getNoRek() {
		return NoRek;
	}

	public void setNoRek(String noRek) {
		NoRek = noRek;
	}

	public String getNama() {
		return Nama;
	}

	public void setNama(String nama) {
		Nama = nama;
	}

}
