package com.bank.mega.bean;

public class SaveInvestorBean {
	private String Sid;
	private String Nama;
	private String NoIdentitas;
	private String TempatLahir;
	private String TglLahir;
	private String KdPekerjaan;
	private String KdJenisKelamin;
	private String KdKota;
	private String Alamat;
	private String NoTelp;
	private String NoHp;
	private String Email;

	public String getSid() {
		return Sid;
	}

	public void setSid(String sid) {
		Sid = sid;
	}

	public String getNama() {
		return Nama;
	}

	public void setNama(String nama) {
		Nama = nama;
	}

	public String getNoIdentitas() {
		return NoIdentitas;
	}

	public void setNoIdentitas(String noIdentitas) {
		NoIdentitas = noIdentitas;
	}

	public String getTempatLahir() {
		return TempatLahir;
	}

	public void setTempatLahir(String tempatLahir) {
		TempatLahir = tempatLahir;
	}

	public String getTglLahir() {
		return TglLahir;
	}

	public void setTglLahir(String tglLahir) {
		TglLahir = tglLahir;
	}

	public String getKdPekerjaan() {
		return KdPekerjaan;
	}

	public void setKdPekerjaan(String kdPekerjaan) {
		KdPekerjaan = kdPekerjaan;
	}

	public String getKdJenisKelamin() {
		return KdJenisKelamin;
	}

	public void setKdJenisKelamin(String kdJenisKelamin) {
		KdJenisKelamin = kdJenisKelamin;
	}

	public String getKdKota() {
		return KdKota;
	}

	public void setKdKota(String kdKota) {
		KdKota = kdKota;
	}

	public String getAlamat() {
		return Alamat;
	}

	public void setAlamat(String alamat) {
		Alamat = alamat;
	}

	public String getNoTelp() {
		return NoTelp;
	}

	public void setNoTelp(String noTelp) {
		NoTelp = noTelp;
	}

	public String getNoHp() {
		return NoHp;
	}

	public void setNoHp(String noHp) {
		NoHp = noHp;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

}
