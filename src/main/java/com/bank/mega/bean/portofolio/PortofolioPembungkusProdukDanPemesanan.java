package com.bank.mega.bean.portofolio;

import java.util.List;

import com.bank.mega.bean.EsbnPemesananEarlyRedemption;
import com.bank.mega.bean.EsbnPemesananTransaksi;

public class PortofolioPembungkusProdukDanPemesanan {
    private String namaSeri;
    private String idSeri;
    private List<EsbnPemesananEarlyRedemption> esbnPemesananEarlyRedemptions;
    private List<EsbnPemesananTransaksi> esbnPemesananTransaksis;
	public String getNamaSeri() {
		return namaSeri;
	}
	public void setNamaSeri(String namaSeri) {
		this.namaSeri = namaSeri;
	}
	public String getIdSeri() {
		return idSeri;
	}
	public void setIdSeri(String idSeri) {
		this.idSeri = idSeri;
	}
	public List<EsbnPemesananEarlyRedemption> getEsbnPemesananEarlyRedemptions() {
		return esbnPemesananEarlyRedemptions;
	}
	public void setEsbnPemesananEarlyRedemptions(List<EsbnPemesananEarlyRedemption> esbnPemesananEarlyRedemptions) {
		this.esbnPemesananEarlyRedemptions = esbnPemesananEarlyRedemptions;
	}
	public List<EsbnPemesananTransaksi> getEsbnPemesananTransaksis() {
		return esbnPemesananTransaksis;
	}
	public void setEsbnPemesananTransaksis(List<EsbnPemesananTransaksi> esbnPemesananTransaksis) {
		this.esbnPemesananTransaksis = esbnPemesananTransaksis;
	}
    
    
}
