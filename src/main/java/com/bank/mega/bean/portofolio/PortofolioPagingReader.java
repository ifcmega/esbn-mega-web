package com.bank.mega.bean.portofolio;

import java.util.List;
import java.util.Map;

import com.bank.mega.bean.EsbnPortofolioDataBeanV2;

public class PortofolioPagingReader {
    private String sid;
    private List<EsbnPortofolioDataBeanV2> dataTable;
    private List<EsbnPortofolioDataBeanV2> dataTableAll;
    private List<String> namaProduks;
    private String kolomTypeTerpilih;
    private String produkTerpilih;
    private Long totalPage;
    private Long pagesize;
    private Long pageNumber;
    private Long totalRecord;
    
    
	
	public String getKolomTypeTerpilih() {
		return kolomTypeTerpilih;
	}
	public void setKolomTypeTerpilih(String kolomTypeTerpilih) {
		this.kolomTypeTerpilih = kolomTypeTerpilih;
	}
	public String getProdukTerpilih() {
		return produkTerpilih;
	}
	public void setProdukTerpilih(String produkTerpilih) {
		this.produkTerpilih = produkTerpilih;
	}
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	
	
	public List<EsbnPortofolioDataBeanV2> getDataTable() {
		return dataTable;
	}
	public void setDataTable(List<EsbnPortofolioDataBeanV2> dataTable) {
		this.dataTable = dataTable;
	}
	public List<EsbnPortofolioDataBeanV2> getDataTableAll() {
		return dataTableAll;
	}
	public void setDataTableAll(List<EsbnPortofolioDataBeanV2> dataTableAll) {
		this.dataTableAll = dataTableAll;
	}
	public List<String> getNamaProduks() {
		return namaProduks;
	}
	public void setNamaProduks(List<String> namaProduks) {
		this.namaProduks = namaProduks;
	}
	public Long getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(Long totalPage) {
		this.totalPage = totalPage;
	}
	public Long getPagesize() {
		return pagesize;
	}
	public void setPagesize(Long pagesize) {
		this.pagesize = pagesize;
	}
	public Long getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Long pageNumber) {
		this.pageNumber = pageNumber;
	}
	public Long getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(Long totalRecord) {
		this.totalRecord = totalRecord;
	}
    
    
    
}
