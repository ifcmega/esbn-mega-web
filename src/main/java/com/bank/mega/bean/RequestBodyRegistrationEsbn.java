package com.bank.mega.bean;

public class RequestBodyRegistrationEsbn {
	private String Sid;
	private String Nama;
	private String NoIdentitas;
	private String TempatLahir;
	private String TglLahir;
	private String KdPekerjaan;
	private String KdJenisKelamin;
	private String KdKota;
	private String Alamat;
	private String NoTelp;
	private String NoHp;
	private String Email;
	private String IdBankDana;
	private String NoRekDana;
	private String IdSubregistry;
	private String IdPartisipan;
	private String NoRekSurat;
	private RekeningDana noRekTerpilih;
	private String oldPhone;
	private String oldEmail;

	public String getOldPhone() {
		return oldPhone;
	}

	public void setOldPhone(String oldPhone) {
		this.oldPhone = oldPhone;
	}

	public String getOldEmail() {
		return oldEmail;
	}

	public void setOldEmail(String oldEmail) {
		this.oldEmail = oldEmail;
	}

	public RekeningDana getNoRekTerpilih() {
		return noRekTerpilih;
	}

	public void setNoRekTerpilih(RekeningDana noRekTerpilih) {
		this.noRekTerpilih = noRekTerpilih;
	}

	private String myOtp;

	public String getSid() {
		return Sid;
	}

	public void setSid(String sid) {
		Sid = sid;
	}

	public String getNama() {
		return Nama;
	}

	public void setNama(String nama) {
		Nama = nama;
	}

	public String getNoIdentitas() {
		return NoIdentitas;
	}

	public void setNoIdentitas(String noIdentitas) {
		NoIdentitas = noIdentitas;
	}

	public String getTempatLahir() {
		return TempatLahir;
	}

	public void setTempatLahir(String tempatLahir) {
		TempatLahir = tempatLahir;
	}

	public String getTglLahir() {
		return TglLahir;
	}

	public void setTglLahir(String tglLahir) {
		TglLahir = tglLahir;
	}

	public String getKdPekerjaan() {
		return KdPekerjaan;
	}

	public void setKdPekerjaan(String kdPekerjaan) {
		KdPekerjaan = kdPekerjaan;
	}

	public String getKdJenisKelamin() {
		return KdJenisKelamin;
	}

	public void setKdJenisKelamin(String kdJenisKelamin) {
		KdJenisKelamin = kdJenisKelamin;
	}

	public String getKdKota() {
		return KdKota;
	}

	public void setKdKota(String kdKota) {
		KdKota = kdKota;
	}

	public String getAlamat() {
		return Alamat;
	}

	public void setAlamat(String alamat) {
		Alamat = alamat;
	}

	public String getNoTelp() {
		return NoTelp;
	}

	public void setNoTelp(String noTelp) {
		NoTelp = noTelp;
	}

	public String getNoHp() {
		return NoHp;
	}

	public void setNoHp(String noHp) {
		NoHp = noHp;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getIdBankDana() {
		return IdBankDana;
	}

	public void setIdBankDana(String idBankDana) {
		IdBankDana = idBankDana;
	}

	public String getNoRekDana() {
		return NoRekDana;
	}

	public void setNoRekDana(String noRekDana) {
		NoRekDana = noRekDana;
	}

	public String getIdSubregistry() {
		return IdSubregistry;
	}

	public void setIdSubregistry(String idSubregistry) {
		IdSubregistry = idSubregistry;
	}

	public String getIdPartisipan() {
		return IdPartisipan;
	}

	public void setIdPartisipan(String idPartisipan) {
		IdPartisipan = idPartisipan;
	}

	public String getNoRekSurat() {
		return NoRekSurat;
	}

	public void setNoRekSurat(String noRekSurat) {
		NoRekSurat = noRekSurat;
	}

	public String getMyOtp() {
		return myOtp;
	}

	public void setMyOtp(String myOtp) {
		this.myOtp = myOtp;
	}

}
