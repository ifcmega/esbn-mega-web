package com.bank.mega.bean;

public class RekeningDana {
	private String idBank;
	private String sid;
	private String noRekening;
	private String nama;
	private String bank;
	private String idBankNorek;
	
	
	public String getIdBankNorek() {
		return idBankNorek;
	}

	public void setIdBankNorek(String idBankNorek) {
		this.idBankNorek = idBankNorek;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getIdBank() {
		return idBank;
	}

	public void setIdBank(String idBank) {
		this.idBank = idBank;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getNoRekening() {
		return noRekening;
	}

	public void setNoRekening(String noRekening) {
		this.noRekening = noRekening;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

}
