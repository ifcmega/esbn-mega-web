package com.bank.mega.bean;

public class EsbnMonitoringPemesanan {
    private String sid;
    private String namaProduk;
    private String sisaKuotaPemesanan;
    private String sisaKuotaSeri;
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	public String getNamaProduk() {
		return namaProduk;
	}
	public void setNamaProduk(String namaProduk) {
		this.namaProduk = namaProduk;
	}
	public String getSisaKuotaPemesanan() {
		return sisaKuotaPemesanan;
	}
	public void setSisaKuotaPemesanan(String sisaKuotaPemesanan) {
		this.sisaKuotaPemesanan = sisaKuotaPemesanan;
	}
	public String getSisaKuotaSeri() {
		return sisaKuotaSeri;
	}
	public void setSisaKuotaSeri(String sisaKuotaSeri) {
		this.sisaKuotaSeri = sisaKuotaSeri;
	}
    
    
}
