var registrationController = angular
		.module('registrationController', [ 'ngSanitize' ])
		.config(
				[
						'$httpProvider',
						function($httpProvider) {
							$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
						} ]).filter('masking', function() {
			return function(input) {
				// //
				// do some bounds checking here to ensure it has that index
				var stuff = input.split('@')[0];
				var host = input.split('@')[1];

				var charStuff = stuff.split('');
				// //

				// var plain = stuff.replace(/[a-zA-Z]/g, 'x');
				var plain = "";
				for (var k = 0; k < charStuff.length; k++) {
					if (k >= charStuff.length - 3) {
						plain += charStuff[k];
					} else {
						plain += 'x';
					}
				}

				return plain + '@' + host;
			}
		})

		.directive(
				'nonNull',
				function() {
					return {
						require : 'ngModel',
						link : function(scope, element, attributes, control) {
							control.$validators.adult = function(modelValue,
									viewValue) {

								if (viewValue == undefined) {
									$(element).css({
										"color" : "red",
										"border-color" : "red"
									})
								} else {
									$(element).css({
										"color" : "black",
										"border-color" : "green"
									})
									return true
								}
							};
						}
					};
				});

registrationController.$inject = [ '$scope' ];

registrationController.directive("datepicker", function() {
	return {
		restrict : "A",
		link : function(scope, el, attr) {
			el.datepicker({
				dateFormat : 'dd/mm/yy'
			});
		}
	};
})

var config = {
	headers : {
		'Accept' : 'application/json, */*'
	}
};

registrationController.controller('testingaja', function($scope, $http,
		$rootScope, $location, $window) {
	$scope.resultGet = function() {
		$scope.response = 'lagi loading';
		$http.get($scope.url).then(
				function(response) {
					var arr = response;

					$scope.response = JSON.stringify(arr);
				}, function error(response) {
					var arr = response;

					$scope.response = JSON.stringify(arr);
					
				});
	}

	$scope.resultPost = function() {
		$scope.response = 'lagi loading';
		$http.post($scope.url,$scope.body).then(
				function(response) {
					var arr = response.data;

					$scope.response = JSON.stringify(arr);
				}, function error(response) {
					var arr = response;

					$scope.response = JSON.stringify(arr);
					
				});
	}
});

registrationController
		.controller(
				'requestLogin',
				function($scope, $http, $rootScope, $location, $window) {

					$scope.checking = true;
					$scope.ektpRegist = '';
					$scope.isAccessPassword = true;
					$scope.isSomethingWrong = false;
					$scope.myData = {};
					$scope.myTimer = '';
					$scope.enableButton = false;
					setInterval(function() {
						$http.get("registration/count-otp").then(
								function(response) {
									// 
									$scope.myTimer = "0:00"
									$scope.enableButton = false;
									if (response.data.valid
											&& response.data.value > 0) {
										$scope.myTimer = "0:"
												+ response.data.value;
										$scope.enableButton = true;
									}
								}, function error(response) {

								});

					}, 1000);

					$scope.observedPassword = function(existing, develop,
							confirm) {

						$scope.url = "registration/change-password/";
						$http.post($scope.url, {
							newOne : develop,
							oldOne : existing,
							confirm : confirm
						}).then(function(response) {

							if (response.data) {
								$scope.isAccessPassword = false;
							} else {
								$scope.isSomethingWrong = true;
							}
						}, function error(response) {

						});

					}

					$scope.observedErrorMessage = function() {
						$http
								.get(
										"master-wording/login/error-wording-searching")
								.then(
										function(response) {
											$scope.errorText = response.data;
										},
										function error(response) {

											$scope.postResultMessage = "Error with status: "
													+ response.statusText;
										});
					}

					$scope.observedErrorMessage();

					$scope.itsSendEmail = {
						validModal : false,
						invalidModal : false,
						waitModal : true
					};

					$scope.sendOtpPlease = function(myId) {
						// 
						$http
								.post("registration/send-my-otp-please", myId)
								.then(
										function(response) {
											if (response.data.valid) {
												// 
												$scope.myData = response.data.userBean;
												$scope.itsSendEmail.validModal = true;
												$scope.itsSendEmail.waitModal = false;
											} else {
												$scope.itsSendEmail.invalidModal = true;
												$scope.itsSendEmail.waitModal = false;
											}
										},
										function error(response) {

											$scope.postResultMessage = "Error with status: "
													+ response.statusText;
										});
					}

					$scope.sendOtpPleaseForgot = function(myId) {
						// //
						if (myId == undefined) {
							$scope.itsSendEmail.invalidModal = true;
							$scope.itsSendEmail.waitModal = false;
							$scope.response = 'Mohon isi Sid Terlebih dahulu';
						} else {
							$http
									.post("registration/otp-forgot", myId)
									.then(
											function(response) {
												// //
												if (response.data.valid) {
													// //
													$scope.myData = response.data.userBean;
													$scope.itsSendEmail.validModal = true;
													$scope.itsSendEmail.waitModal = false;
												} else {
													// //
													$scope.itsSendEmail.invalidModal = true;
													$scope.itsSendEmail.waitModal = false;
													$scope.response = response.data.reason;
												}
											},
											function error(response) {

												$scope.postResultMessage = "Error with status: "
														+ response.statusText;
											});
						}
					}

					$scope.okOtp = function() {
						$scope.itsSendEmail = {
							validModal : false,
							invalidModal : false,
							waitModal : true
						};
					}

					$scope.itsValidIdentity = {
						validModal : false,
						invalidModal : false,
						waitModal : true
					};

					$scope.callMyIdentity = function(myId) {
						$http
								.post("registration/see-identityCard", myId)
								.then(
										function(response) {

											if (response.data.valid) {
												$scope
														.generateCaptcha('signup1');
												// 
												$scope.myData = response.data.userBean;
												$scope.ektpRegist = response.data.userBean.ektp;
												$scope.itsValidIdentity.validModal = true;
												$scope.itsValidIdentity.waitModal = false;
											} else {
												$scope.itsValidIdentity.invalidModal = true;
												$scope.itsValidIdentity.waitModal = false;
												$scope.reasonError = response.data.reason;
											}
										},
										function error(response) {

											$scope.postResultMessage = "Error with status: "
													+ response.statusText;
										});
					}

					$scope.cancelRegist = function() {
						$scope.myData = {};
						$scope.itsValidIdentity = {
							validModal : false,
							invalidModal : false,
							waitModal : true
						};
						$scope.captchaEscape = '';
						$scope.button1 = true;
						$scope.itsSendEmail = {
							validModal : false,
							invalidModal : false,
							waitModal : true
						};

					}

					$scope.itsWinningSave = {
						validModal : false,
						invalidModal : false,
						waitModal : true
					};

					$scope.okSaveData = function() {
						$scope.itsWinningSave = {
							validModal : false,
							invalidModal : false,
							waitModal : true
						};
					}

					$scope.masterWording = function() {
						$http
								.post("master-wording/disclaimer", 'DISC1VERS1')
								.then(
										function(response) {
											// //
											$scope.disclaimer1 = response.data;
										},
										function error(response) {
											$scope.postResultMessage = "Error with status: "
													+ response.statusText;
										});
						$http
								.post("master-wording/disclaimer", 'DISC1VERS2')
								.then(
										function(response) {
											// //
											$scope.disclaimer2 = response.data;
										},
										function error(response) {
											$scope.postResultMessage = "Error with status: "
													+ response.statusText;
										});
					}

					$scope.masterWording();

					$scope.saveMySid = function(mySid) {
						$scope.okSaveData();
						$http
								.post("registration/registered-sid", mySid)
								.then(
										function(response) {
											// //

											if (response.data.valid) {
												$scope.itsWinningSave.validModal = true;
												$scope.itsWinningSave.waitModal = false;
												$scope.myDataForgot = {};
												$scope.cancelRegist();
											} else {
												// //
												$scope.itsWinningSave.invalidModal = true;
												$scope.reasonInvalid = response.data.reason;
												$scope.itsWinningSave.waitModal = false;
											}

											// if(response.data=="true"){
											// $scope.itsWinningSave.validModal
											// = true;
											// $scope.itsWinningSave.waitModal =
											// false;
											// $scope.cancelRegist();
											// }
											// else{
											// ////
											// $scope.itsWinningSave.invalidModal
											// = true;
											// $scope.itsWinningSave.waitModal =
											// false;
											// }
										},
										function error(response) {
											$scope.postResultMessage = "Error with status: "
													+ response.statusText;
										});
					}

					// start update password
					$scope.saveForgotPassword = function() {
						// //
						$scope.okSaveData();
						$scope.hashPass = {
							newPass : $scope.myDataForgot.newPass,
							confirmPass : $scope.myDataForgot.confirmPass,
							tokenOtp : $scope.myDataForgot.otpChecker,
							realTokenOtp : $scope.myData.otpVerification,
							needSid : $scope.myDataForgot.sid
						};

						$http
								.post("registration/update-password",
										$scope.hashPass)
								.then(
										function(response) {
											// //
											if (response.data.valid) {
												$scope.itsWinningSave.validModal = true;
												$scope.itsWinningSave.waitModal = false;
												$scope.myDataForgot = {};
												$scope.cancelRegist();
											} else {
												// //
												$scope.itsWinningSave.invalidModal = true;
												$scope.reasonInvalid = response.data.reason;
												$scope.itsWinningSave.waitModal = false;
											}
										},
										function error(response) {
											$scope.postResultMessage = "Error with status: "
													+ response.statusText;
											$scope.itsWinningSave.invalidModal = true;
											$scope.itsWinningSave.waitModal = false;
										});
					}

					// end update password
					// start ubah password dari dalam
					$scope.validChange = false;
					$scope.saveUbahPassword = function() {
						$scope.myDataUbah;

						$http
								.post("registration/update-password/no-forgot",
										$scope.myDataUbah)
								.then(
										function(response) {

											$scope.validChange = true;
											$scope.reason = response.data.reason;
										},
										function error(response) {
											$scope.postResultMessage = "Error with status: "
													+ response.statusText;
										});
					}
					// start ubah password dari luar

					// start captcha

					$scope.captchaEscape = '';
					$scope.button1 = true;
					$scope.button2 = true;
					$scope.button3 = true;
					$scope.yourCaptcha = '';
					$scope.generateCaptcha = function(idButton) {
						$scope.emptyCaptcha = true;
						$scope.wrongCaptcha = false;
						$scope.yourCaptcha = '';
						if (idButton == 'signup1') {
							$scope.button1 = true;

							var captcha;
							var a = Math.floor((Math.random() * 10));
							var b = Math.floor((Math.random() * 10));
							var c = Math.floor((Math.random() * 10));
							var d = Math.floor((Math.random() * 10));
							var e = Math.floor((Math.random() * 10));
							var f = Math.floor((Math.random() * 10));

							captcha = a.toString() + b.toString()
									+ c.toString() + d.toString()
									+ e.toString() + f.toString();

							$scope.captchaEscape = captcha;
						} else if (idButton == 'signup2') {

							var captcha;
							var a = Math.floor((Math.random() * 10));
							var b = Math.floor((Math.random() * 10));
							var c = Math.floor((Math.random() * 10));
							var d = Math.floor((Math.random() * 10));
							var e = Math.floor((Math.random() * 10));
							var f = Math.floor((Math.random() * 10));

							captcha = a.toString() + b.toString()
									+ c.toString() + d.toString()
									+ e.toString() + f.toString();
							$scope.captchaEscape2 = captcha;
						}
					}

					$scope.checkCaptcha = function(idButton, captchaValue,
							yourValue, checking) {
						////

						if (idButton = 'signup1') {

							if (captchaValue == yourValue) {
								$scope.emptyCaptcha = false;
								$scope.wrongCaptcha = false;
								if (checking) {
									$scope.button1 = false;
								} else {
									$scope.button1 = true;
								}
							} else {
								$scope.emptyCaptcha = false;
								$scope.wrongCaptcha = true;
								$scope.button1 = true;
							}

						} else if (idButton = 'signup2') {
							if (captchaValue == yourValue) {

								$scope.button2 = false;
							} else {
								$scope.button2 = true;
							}
						} else if (idButton = 'signup3') {
							if (captchaValue == yourValue) {

								$scope.button3 = false;
							} else {
								$scope.button3 = true;
							}
						}
					}

					//end captcha

				});
