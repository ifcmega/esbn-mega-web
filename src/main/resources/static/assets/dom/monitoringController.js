var endpoints = location.origin;
var monitoringController = angular
		.module('monitoringController', [])
		.config(
				[
						'$httpProvider',
						function($httpProvider) {
							$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
						} ]).directive('format', ['$filter', function ($filter) {
						    return {
						        require: '?ngModel',
						        link: function (scope, elem, attrs, ctrl) {
						            if (!ctrl) return;


						            ctrl.$formatters.unshift(function (a) {
						                return $filter(attrs.format)(ctrl.$modelValue)
						            });


						            ctrl.$parsers.unshift(function (viewValue) {
						                var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
						                elem.val($filter(attrs.format)(plainNumber));
						                return plainNumber;
						            });
						        }
						    };
						}])
						.directive('nonNulldrop', function () {
	        return {
	            require: 'ngModel',
	            link: function (scope, element, attributes, control) {
	                control.$validators.adult = function (modelValue, viewValue) {
                         
                         if(viewValue==undefined){
                         $(element).css({"border-color" : "red"})
                         }
                         else{
                         $(element).css({"color":"black","border-color" : "green"}) 
                         return true
                         }
	                };
	            }
	        };
	    });



var config = {
	headers : {
		'Accept' : 'application/json, */*'
	}
};

monitoringController.controller('monitoringPemesanan', function($scope, $http,
		$rootScope, $location, $window) {
	$scope.tester="hahah";
	
	$scope.getAllMySeri = function(){
		$http.get("monitoring/get-all-seri", config)
		.then(
				function(response) {
				   $scope.listSeries = response.data.allSeriProducts;
				   $scope.esbnInfoUser = response.data.esbnInfoUser;
				},
				function error(response) {
					
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
	}
	
	$scope.getAllMySeri();
	$scope.pageNumber = 1;
/*	$scope.selectedSeri = function(){
		$scope.esbnInfoUser;
		$scope.listData = undefined;
		$http.post("monitoring/getKuotaSeri",
				$scope.esbnInfoUser.seriTerpilih.id)
		.then(
				function(response) {
					//debugger;
				   $scope.esbnInfoUser.sisaKuotaPemesanan = response.data.KuotaInvestor;
				   $scope.esbnInfoUser.sisaKuotaSeri = response.data.KuotaSeri;
				   $scope.searchMyData($scope.pageNumber);
				},
				function error(response) {
					
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
	}*/
	$scope.searchMyData = function(page){
		//debugger;
		$scope.esbnInfoUser;
		if($scope.esbnInfoUser.seriTerpilih == undefined ){
			alert('Mohon pastikan Anda sudah memilih produk');
		}else{
			
			$scope.mapping = {					
					idSeri : $scope.esbnInfoUser.seriTerpilih.id,
					pages : page					
			};
		$http.post("monitoring/search-monitoringPemesanan",$scope.mapping)
		.then(
				function(response) {
					//debugger;
					response.data
					$scope.listData = response.data.listEsbnPemesananTrx;
					$scope.pageNumber = response.data.pageNumber;
					$scope.pagesize = response.data.pagesize;
					$scope.totalPage = response.data.totalPage;
					$scope.totalRecord = response.data.totalRecord;
					
				},
				function error(response) {
					
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
		
		}
	}
 
	

	
});

monitoringController.controller('monitoringEarlyRedemption', function($scope, $http,
		$rootScope, $location, $window) {
	$scope.tester="hahah";
	
	$scope.getAllMyRedeem = function(){
		$http.get("monitoring/get-all-seriRedeem", config)
		.then( 
				function(response) {
					
				   $scope.listSeries = response.data.allSeriProducts;
				   $scope.esbnInfoUser = response.data.esbnInfoUser;
				},
				function error(response) {
					
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
		
	}
	
	$scope.getAllMyRedeem();
	$scope.pageNumberRedeem = 1;
	/*$scope.selectedSeriRedeem = function(){
		$scope.esbnInfoUser;
		$scope.listDatas = undefined;
		//debugger;
		
		$http.post("monitoring/getKuotaRedeem",
				$scope.esbnInfoUser.seriTerpilih.id)
		.then(
				function(response) {
					//debugger;
				   $scope.esbnInfoUser.sisaKuotaPemesanan = response.data.KuotaInvestor;
				   $scope.esbnInfoUser.sisaKuotaSeri = response.data.KuotaSeri;
				   $scope.searchMyDataEarly($scope.pageNumberRedeem);
				},
				function error(response) {
					
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
		
	}*/
	$scope.searchMyDataEarly = function(page){
		$scope.esbnInfoUser;
		if($scope.esbnInfoUser.seriTerpilih == undefined ){
			alert('Mohon pastikan Anda sudah memilih produk');
		}else{
			
			$scope.mapping = {					
					idSeri : $scope.esbnInfoUser.seriTerpilih.id,
					pages : page					
			};
		$http.post("monitoring/search-monitoringEarlyRedemption",
				$scope.mapping)
		.then(
				function(response) {
					$scope.listDatas = response.data.listEsbnPemesananEarlyTrx;
					$scope.pageNumberRedeem = response.data.pageNumberEarly;
					$scope.pagesize = response.data.pagesizeEarly;
					$scope.totalPage = response.data.totalPageEarly;
					$scope.totalRecord = response.data.totalRecordEarly;
					
					
				},
				function error(response) {
					
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
		
		}
	}
	
});